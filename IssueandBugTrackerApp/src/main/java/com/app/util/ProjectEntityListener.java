package com.app.util;

import java.util.List;

import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.app.entities.Project;
import com.app.repositories.ProjectRepository;

/**
 * @author Deepanjan
 */
public class ProjectEntityListener {
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	ProjectRepository projectRepository;

	@PrePersist
	public void setProjectId(Project project) throws Exception{

		InjectionHelper.autowire(this, this.authUtilService);
		List<Project> projectExists = projectRepository.findByTenant(authUtilService
				.getLoggedInUsersTenant());
		if (!projectExists.isEmpty()) {
			long maxId = projectRepository.findByTenantMaxProjectId(authUtilService
					.getLoggedInUsersTenant());
			project.setProjectId((maxId) + 1);
		} else {
			project.setProjectId(1);
		}

	}

}
