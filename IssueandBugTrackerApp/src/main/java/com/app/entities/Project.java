package com.app.entities;
/**
 * @author Deepanjan
 */
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.app.util.ProjectEntityListener;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.TaskJsonView;
import com.app.util.views.UserJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "projects")
@EntityListeners(ProjectEntityListener.class)
public class Project extends AbstractTenantEntity {

	private static final long serialVersionUID = -7831937129948683966L;
	
	@Column(name = "project_id", updatable = false)		
	@JsonView({ UserJsonView.class, ProjectJsonView.class, TaskJsonView.class,		
			TaskJsonView.Summary.class })		
	private long projectId;
	

	@Column(name = "project_name")
	@JsonView({ UserJsonView.class, ProjectJsonView.class, TaskJsonView.class, TaskJsonView.Summary.class })
	private String projectName;

	@Column(name = "description")
	@JsonView({ UserJsonView.class, ProjectJsonView.class })
	private String description;
	
	@Column(name = "start_date")
	@JsonView({ UserJsonView.class, ProjectJsonView.class })
	private Date startDate;

	@OneToMany(mappedBy = "project")
	@JsonView({ ProjectJsonView.class })
	List<Task> tasks;

	@OneToMany(mappedBy = "project")
	@JsonView({ ProjectJsonView.class })
	List<Tag> tags;
	
	public long getProjectId() {		
		return projectId;		
	}		
	public void setProjectId(long projectId) {		
		this.projectId = projectId;		
	}


	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
