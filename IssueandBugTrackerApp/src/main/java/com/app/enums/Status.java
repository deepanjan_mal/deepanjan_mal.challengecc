package com.app.enums;

/**
 * @author Deepanjan
 */
public enum Status {
	
	ACTIVE("Active"),
	IN_PROGRESS("In Progress"),
	PAUSED("Paused"),
	COMPLETED("Completed"),
	CLOSE("Close");

	
	String value;

	/**
	 * @param value
	 */
	private Status(String value) {
		this.value = value;
	}
	
	public String value() {
		return this.value;
	}

	public String toString(){
		return this.value;
	}
	
}
