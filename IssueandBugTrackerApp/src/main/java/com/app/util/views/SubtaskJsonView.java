
package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;


public class SubtaskJsonView {

	public interface Summary {
	};

	public interface SubtaskReadView extends PaginatedResult {
	};
}
