package com.app.enums;

public enum Authority {

	CREATE_ISSUE("Create Issue"), 
	
	EDIT_ISSUE("Edit Issue"),
	
	ADDING_SUBTASK("Adding Subtask"), 
	
	ADD_ASSIGNEE("Add Assignees"), 
	
	DELETE_ISSUE("Delete Issue"), 
	
	CLOSE_ISSUE("Close Issue"),
	
	//CREATE_PROJECT("Create Project"), 
	
	EDIT_PROJECT("Edit Project"), 
	
	ADD_MEMBER("Add Members"),
	
	EDIT_SUBTASK("Edit Subtask"), 
	
	DELETE_SUBTASK("Delete Subtask");

	String value;

	private Authority(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public String toString() {
		return this.value;
	}
}
