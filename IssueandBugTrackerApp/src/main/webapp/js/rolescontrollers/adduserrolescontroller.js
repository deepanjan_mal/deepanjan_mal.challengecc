tmsApp.controller('adduserrolescontroller',
		function($scope, $rootScope, $http, getusersservice, tmsutility,
				toaster, ngToast, $routeParams, $timeout, $log) {
			tmsutility.method($scope);
			$scope.projectId = $routeParams.projectId;
			$scope.getUser = function() {
				$http.get(
						'/all/project/getUserForProject?value='
								+ $routeParams.projectId).success(
						function(response) {
							$scope.users = response;
						}).error(function(err) {

				})
			}
			$scope.getUser();
			$http.get('/all/project/getRoles').success(function(response) {
				$scope.roles = response;
			}).error(function(err) {
				$log.info(err);
			})

			$scope.addUserProjectRoles = function() {
				$scope.userprojectRoles = {
					user : $scope.userProjectRoles.originalObject,
					project : {
						projectId : $routeParams.projectId
					},
					projectRole : $scope.userProjectRoles.role
				}
				if ($scope.userprojectRoles.user) {

					$http.post('/all/project/userprojectroles',
							$scope.userprojectRoles)
							.success(
									function() {
										$scope.getUserProjectRoles();
										// success
										toaster.method("success", "Success!!",
												ngToast);
										$scope.getUser();
										$rootScope.getProjects();
										$scope.userProjectRoles = "";
										$scope.$broadcast(
												'angucomplete-alt:clearInput',
												'users', null);
									}).error(function(err) {
								$log.info(err);
								// error
								toaster.method("danger", "Error!!", ngToast);
							})
				}
			}
			$scope.getUserProjectRoles = function() {

				$http.get(
						'/all/project/getuserprojectroles?value='
								+ $routeParams.projectId).success(
						function(response) {

							$scope.userProjectRoleIterator = response;
						}).error(function(err) {
					$log.info(err);
				})
			}

			if ($rootScope.isAuthenticated) {
				$scope.getUserProjectRoles();
			}
			$scope.passRole = function(role) {
				$scope.userProjectRole = angular.copy(role);

			}

			$scope.updateUserProjectRoles = function() {
				$scope.edituserProjectRoles = {
					user : $scope.userProjectRole.user,
					project : {
						projectId : $routeParams.projectId
					},
					projectRole : $scope.userProjectRole.projectRole
				}
				$http.post('/all/project/updateuserprojectroles',
						$scope.edituserProjectRoles).success(function() {
					$scope.getUserProjectRoles();
					// success
					toaster.method("success", "Success!!", ngToast);
				}).error(function(err) {
					// error
					$log.info(err);
					toaster.method("danger", "Error!!", ngToast);
				})
			}

			$scope.deleteUserProjectRoles = function(role) {

				$scope.deleteuserProjectRoles = {
					id : role.id

				}
				$http.post('/all/project/deleteuserprojectroles',
						$scope.deleteuserProjectRoles).success(function() {
					$scope.getUserProjectRoles();
					// success
					toaster.method("success", "Success!!", ngToast);
					$scope.getUser();
					$rootScope.getProjects();
				}).error(function() {
					// error
					toaster.method("danger", "Error!!", ngToast);
				})
			}

		})