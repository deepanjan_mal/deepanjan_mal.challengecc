tmsApp.factory("taskFactory", function(taskServices, commentServices,
		subtaskServices, edittaskServices, fileServices) {

	var Task = function() {
	}

	Task.prototype = {

		searchTasks : function() {
			return taskServices.searchTasks();
		},

		getHistory : function(taskId) {
			return taskServices.getHistory(taskId);
		},

		editTitle : function(task) {
			return edittaskServices.title(task)
		},

		editDescription : function(task) {
			return edittaskServices.description(task)
		},

		changeStatus : function(status) {
			return edittaskServices.status(status)
		},

		changePriority : function(priority) {
			return edittaskServices.priority(priority)
		},

		changestartDate : function(date) {
			return edittaskServices.startDate(date)
		},

		changeDeadline : function(date) {
			return edittaskServices.deadline(date)
		},

		addUsers : function(users) {

			return edittaskServices.users(users)
		},

		addTags : function(tag) {
			return edittaskServices.tag(tag)
		},

		addComment : function(projectId, taskId, title) {
			return commentServices.addComment(projectId, taskId, title)
		},

		editComment : function(projectId, taskId, commentId, message) {
			return commentServices.editComment(projectId, taskId, commentId,
					message)
		},

		deleteComment : function(projectId, taskId, commentId) {
			return commentServices.deleteComment(projectId, taskId, commentId)
		},

		addSubtask : function(projectId, taskId, subtasks) {
			return subtaskServices.addSubtask(projectId, taskId, subtasks);
		},

		deleteSubtask : function(projectId, taskId, subtaskId) {
			return subtaskServices.deleteSubtask(projectId, taskId, subtaskId);
		},

		editSubtask : function(projectId, taskId, subtaskId, subtasks) {
			return subtaskServices.editSubtask(projectId, taskId, subtaskId,
					subtasks);
		},

		changeSubtaskStatus : function(projectId, taskId, subTaskId, status) {
			return subtaskServices.changeSubtaskStatus(projectId, taskId,
					subTaskId, status);
		},

		uploadFile : function(FileUploader, $scope, url) {
			return fileServices.uploadFile(FileUploader, $scope, url);
		},

		getFiles : function(projectId, taskId) {
			return fileServices.getFiles(projectId, taskId)
		},

		downloadFile : function(projectId, taskId, filename) {
			return fileServices.downloadFile(projectId, taskId, filename);
		},

		downloadAllFile : function(projectId, taskId, filename) {
			return fileServices.downloadAllFile(projectId, taskId, filename)
		},

		deleteFile : function(projectId, taskId, filename) {
			return fileServices.deleteFile(projectId, taskId, filename);
		}

	};

	return Task;
})