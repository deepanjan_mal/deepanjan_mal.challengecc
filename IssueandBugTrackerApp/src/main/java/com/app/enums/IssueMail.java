package com.app.enums;

import org.apache.commons.lang.text.StrBuilder;

public enum IssueMail {
	MESSAGE("<br>Issue is being raised and assigned to you, please login and click on this url to view:" + "<a href='" + "link" + "'>" + "link" + "</a>");

	IssueMail(String template) {
		this.template = template;
	}

	String template;

	/**
	 * Generate a mail template for activation of a new user with activation
	 * link and reset password with forgot password link
	 * 
	 * @param token
	 * @param link
	 * @return String
	 */
	public String getMessage(String link) {
		return new StrBuilder(template).replaceAll("link", link.toString()).toString();
	}

}
