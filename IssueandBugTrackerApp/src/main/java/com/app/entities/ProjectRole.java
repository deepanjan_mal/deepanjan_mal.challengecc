package com.app.entities;
/**
 * @author Deepanjan
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.app.enums.Authority;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.ProjectRoleJsonView;
import com.app.util.views.UserJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "project_roles", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"role", "tenant_id" }) })
public class ProjectRole extends AbstractTenantEntity {

	private static final long serialVersionUID = 4015017870432870755L;

	@Column(name = "role")
	@JsonView({ ProjectJsonView.class, UserJsonView.class,
			UserProjectRoleJsonView.class, ProjectRoleJsonView.class })
	private String role;

	@OneToMany(mappedBy = "role")
	List<ProjectAuthority> authorities;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ProjectAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<ProjectAuthority> authorities) {
		this.authorities = authorities;
	}

	@JsonIgnore
	public boolean containsAuthority(Authority authority) {
		for (ProjectAuthority projectAuthority : authorities) {
			if (projectAuthority.getAuthority().equals(authority)) {
				return true;
			}
		}
		return false;
	}

	@JsonView({ ProjectRoleJsonView.class })
	public Map<Authority, String> getAuth() {
		HashMap<Authority, String> listOfAuthorities = new HashMap<Authority, String>();
		for (ProjectAuthority projectAuthority : authorities) {
			listOfAuthorities.put(projectAuthority.getAuthority(),
					projectAuthority.getAuthority().toString());
		}
		return listOfAuthorities;
	}

}
