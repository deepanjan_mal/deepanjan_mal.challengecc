package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.Tenant;


public interface TenantRepository extends JpaRepository<Tenant, Long> {

	Tenant findByDomain(String domain);
	
	Tenant findByTenantName(String tenantName);
	
	List<Tenant> findByTenantNameIgnoreCaseContaining(String tenantName);
	

}
