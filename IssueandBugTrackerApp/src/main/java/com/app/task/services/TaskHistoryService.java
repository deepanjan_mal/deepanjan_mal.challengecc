package com.app.task.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.entities.Task;
import com.app.entities.audit.TaskHistory;
import com.app.enums.TaskAudit;
import com.app.repositories.TaskPropertyRepository;

/**
 * @author deepanjan
 *
 */

@Service
public class TaskHistoryService {

	@Autowired
	TaskPropertyRepository taskPropertyRepository;

	public void addAuditUtility(TaskAudit taskAudit, Task task,
			String newValue, String oldValue) throws IllegalArgumentException,
			IllegalAccessException {
		TaskHistory taskProperty = new TaskHistory();
		
		taskProperty.setAuditTaskProperty(taskAudit);
		taskProperty.setNewValue(newValue);
		if (oldValue != null)
			taskProperty.setOldValue(oldValue);
		taskProperty.setTask(task);
		taskPropertyRepository.save(taskProperty);

	}

	public void addAuditUtility(TaskAudit taskAudit, Task task, String newValue)
			throws IllegalArgumentException, IllegalAccessException {
		TaskHistory taskProperty = new TaskHistory();
	
		taskProperty.setAuditTaskProperty(taskAudit);
		taskProperty.setNewValue(newValue);
		taskProperty.setTask(task);
		taskPropertyRepository.save(taskProperty);

	}

	public void tagAuditUtility(TaskHistory taskProperty, TaskAudit taskAudit,
			Task task, String tag) throws IllegalArgumentException,
			IllegalAccessException {
		
		taskProperty.setAuditTaskProperty(taskAudit);
		taskProperty.setNewValue(tag);
		taskProperty.setTask(task);
		taskPropertyRepository.save(taskProperty);
	}
}