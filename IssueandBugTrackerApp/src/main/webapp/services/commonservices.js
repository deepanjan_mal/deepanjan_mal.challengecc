tmsApp.factory('createtask', function($http) {
	return {
		task : function($scope, task) {
			return $http.post('/all/task/addtask', task);
		}
	}
});

tmsApp.factory('getusersservice', function($http) {
	return {
		method : function() {
			return $http.get('/all/getUsers?value=');
		}
	}
});

tmsApp.factory('toaster', function() {
	return {
		method : function(classname, content, ngToast) {
			ngToast.create({
				className : classname,
				content : content
			});
		}
	}
})

tmsApp.factory('setstatusicon', function() {
	return {
		getIcon : function(status) {

			if (status == "In Progress") {
				return "glyphicon glyphicon-play"
			} else if (status == "Paused") {

				return "glyphicon glyphicon-pause"
			} else if (status == "Completed") {
				return "glyphicon glyphicon-ok-circle"
			} else if (status == "Close") {
				return "glyphicon glyphicon-ok-sign"
			} else if (status == "Active") {
				return "glyphicon glyphicon-play-circle"
			}

		}

	}
})

tmsApp
		.factory(
				'getClass',
				function(setstatusicon, getstring) {
					return {
						getclass : function(msg) {

							var message = msg;
							if (getstring.getString(message, "Status") == "Status") {
								if (message.match(/Active/)) {
									return setstatusicon.getIcon(message
											.match(/Active/));
								} else if (message.match(/In Progress/)) {
									return setstatusicon.getIcon(message
											.match(/In Progress/));
								} else if (message.match(/Completed/)) {
									return setstatusicon.getIcon(message
											.match(/Completed/));
								} else if (message.match(/Close/)) {

									return setstatusicon.getIcon(message
											.match(/Close/));
								} else if (message.match(/Paused/)) {
									return setstatusicon.getIcon(message
											.match(/Paused/));
								}
							} else if (getstring.getString(message, "Priority") == "Priority") {
								if (message.match(/High/)) {
									return "glyphicon glyphicon-star High";
								} else if (message.match(/Critical/)) {
									return "glyphicon glyphicon-star Critical";
								} else if (message.match(/Medium/)) {
									return "glyphicon glyphicon-star Medium";
								} else if (message.match(/Low/)) {
									return "glyphicon glyphicon-star Low";
								} else if (message.match(/None/)) {
									return "glyphicon glyphicon-star";
								}

							} else if (getstring.getString(message, "Comments") == "Comments") {
								return "glyphicon glyphicon-cloud";
							} else if (getstring.getString(message, "Files") == "Files") {
								return "glyphicon glyphicon-paperclip";
							} else if (getstring.getString(message, "Tags") == "Tags") {
								return "glyphicon glyphicon-tags";
							} else if (getstring.getString(message, "Subtasks") == "Subtasks") {
								return "glyphicon glyphicon-tasks glyphicon";
							} else if ((getstring.getString(message, "Title") == "Title")
									|| (getstring.getString(message,
											"Description") == "Description")) {
								return "glyphicon glyphicon-pencil";
							} else if ((getstring.getString(message,
									"Start date") == "Start")
									|| (getstring.getString(message,
											"Complete date") == "Complete")
									|| (getstring
											.getString(message, "Deadline") == "Deadline")) {
								return "glyphicon glyphicon-calendar";
							} else {
								return "glyphicon glyphicon-user";
							}
						}
					}

				})

tmsApp.factory('getstring', function() {
	return {

		getString : function(str, message) {
			var s = str.substring(0, str.indexOf(" "));
			return s;
		}
	}

})
tmsApp.factory('getGlyphicons', function() {

	return {
		priority : function(key) {
			switch (key) {
			case 'HIGH':
				return {
					"glyphicon" : 'glyphicon glyphicon-star High',
					"index" : 1
				}
			case 'MEDIUM':
				return {
					"glyphicon" : 'glyphicon glyphicon-star Medium',
					"index" : 2
				}
			case 'LOW':
				return {
					"glyphicon" : 'glyphicon glyphicon-star Low',
					"index" : 3
				}
			case 'CRITICAL':
				return {
					"glyphicon" : 'glyphicon glyphicon-star Critical',
					"index" : 0

				}
			case 'NONE':
				return {
					glyphicon : 'glyphicon glyphicon-star None',
					"index" : 4
				}

			}
		},

		status : function(key) {
			switch (key) {
			case 'ACTIVE':
				return {
					"glyphicon" : 'glyphicon glyphicon-play-circle',
					"index" : 0
				}
			case 'IN_PROGRESS':
				return {
					"glyphicon" : 'glyphicon glyphicon-play',
					"index" : 1

				}

			case 'PAUSED':
				return {
					"glyphicon" : 'glyphicon glyphicon-pause',
					"index" : 2
				}
			case 'COMPLETED':
				return {
					"glyphicon" : 'glyphicon glyphicon-ok-circle',
					"index" : 3
				}
			case 'CLOSE':
				return {
					"glyphicon" : 'glyphicon glyphicon-ok-sign',
					"index" : 4
				}
			}
		}
	}
})

tmsApp.service('userInfo', function($http) {
	return {
		getUserInfo : function(username) {
			return $http.get('/all/getUserDetail?username=' + username);
		}
	}
})

tmsApp.directive("enterpress", function() {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {

			element.bind("keyup", function(event) {

				event.keyCode == 13 ? scope.$apply(attrs.enterpress) : false;

			})
		}
	}
})
tmsApp.directive("outfocus", function() {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			element.bind("blur", function(event) {
				event.type == "blur" ? scope.$apply(attrs.outfocus) : false;

			})
		}
	}
})
tmsApp.directive("keypress", function() {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			element.bind("keypress", function(event) {
				event.type == "keypress" && event.keyCode != 13 ? scope
						.$apply(attrs.keypress) : false;

			})
		}
	}
})