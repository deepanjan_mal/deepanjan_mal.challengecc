package com.app.util;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.app.entities.Tenant;
import com.app.entities.User;

/**
 * Utilites service related to authentication/authroization. This service will
 * be autowired in all the other services wherever information related to user
 * details are needed.
 * 
 * @author Deepanjan
 *
 */
@Service
public class AuthUtilService {

	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Returns the tenant of the logged in user
	 * 
	 * @return
	 */
	public User getLoggedInUser() {
		logger.info("Returning the logged in user object");
		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		return user;
	}

	/**
	 * Returns the tenant of the logged in user's tenant
	 * 
	 * @return
	 */

	public Tenant getLoggedInUsersTenant() {
		logger.info("Returning logged in user's tenant");
		return getLoggedInUser().getTenant();
	}

	/**
	 * Returns the tenant of the logged in user's tenant domain name
	 * 
	 * @return
	 */
	public String getLoggedInUsersTenantName() {
		logger.info("Returning logged in user's tenant");
		return getLoggedInUser().getTenant().getDomain();
	}

}
