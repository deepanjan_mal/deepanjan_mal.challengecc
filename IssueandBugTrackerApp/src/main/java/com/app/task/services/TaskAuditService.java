package com.app.task.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.entities.audit.TaskHistory;
import com.app.enums.TaskAudit;
import com.app.repositories.TaskHistoryRepository;
import com.app.repositories.TaskRepository;
import com.app.util.AuthUtilService;

@Service
public class TaskAuditService {

	@Autowired
	TaskHistoryRepository taskHistoryRepository;

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	AuthUtilService authUtilService;

	public Logger log = LoggerFactory.getLogger(getClass());

	public TaskAudit taskAudit;

	/**
	 * Returns the History for a particular task
	 * 
	 * @param task
	 * @return List
	 * @throws Exception
	 */
	public List<String> getAudit(Long task) throws Exception {
		HashMap<String, List<String>> returnMessage = new HashMap<String, List<String>>();
		List<String> returnTaskHistory = new ArrayList<String>();
		List<TaskHistory> taskHistory = taskHistoryRepository
				.findByTaskId(taskRepository.findByTaskIdAndTenant(task,
						authUtilService.getLoggedInUsersTenant()).getId());

		for (TaskHistory taskhistory : taskHistory) {

			String historyTemplate = taskhistory.getAuditTaskProperty()
					.getAuditMessage(taskhistory);
			if (historyTemplate != null) {
				returnTaskHistory.add(historyTemplate);
			} else {
				log.info("Property is set to null");
			}
		}
		returnMessage.put("message", returnTaskHistory);
		return returnTaskHistory;
	}
}
