/**
 * 
 */
package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.Status;

public interface StatusRepository extends JpaRepository<Status, String> {

}
