package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entities.Project;
import com.app.entities.User;
import com.app.entities.UserProjectRole;

@Repository
public interface UserProjectRolesRepository extends
		JpaRepository<UserProjectRole, String> {

	public UserProjectRole findByProjectAndUser(Project project, User user);

	public List<Project> findByUser(User user);

	public List<UserProjectRole> findByProjectId(String id);

	public List<UserProjectRole> findByProject(Project project);

	/**
	 * @param projectId
	 * @param id
	 * @return
	 */
	public List<UserProjectRole> findByProjectIdAndUserId(String projectId,
			String id);

	public List<UserProjectRole> findByUserId(String id);

	public UserProjectRole findById(String id);


	

}
