package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.app.entities.audit.TaskHistory;
import com.app.enums.Priority;
import com.app.enums.Status;
import com.app.util.TaskEntityListener;
import com.app.util.views.LoadUserJsonVew;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.TaskJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tasks")
@EntityListeners(TaskEntityListener.class)
public class Task extends AbstractTenantEntity  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "task_id", updatable = false)
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	long taskId;

	@Column(name = "title")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	private String title;

	@Column(name = "status")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "priority")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	@Enumerated(EnumType.STRING)
	private Priority priority;

	@Column(name = "description")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class })
	String description;

	@Column(name = "start_date")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	Date startDate;

	@Column(name = "complete_date")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	Date completeDate;

	@Column(name = "deadline")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class })
	Date deadline;

	@Column(name = "isdelete")
	@JsonView({ TaskJsonView.class, ProjectJsonView.class })
	boolean isDelete = false;

	@OneToMany(mappedBy = "task")
	@JsonView({ TaskJsonView.class })
	List<Comment> comments;

	@ManyToMany
	@JoinTable(name = "tasks_users", joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	@JsonView({ TaskJsonView.class})
	List<User> users;
	
	@ManyToMany
	@JoinTable(name = "tasks_users_subscribers", joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	@JsonView({ TaskJsonView.class })
	List<User> usersSubscribers;

	@ManyToOne
	@JsonView({ TaskJsonView.class, TaskJsonView.Summary.class })
	Project project;

	@OneToMany(mappedBy = "task")
	@JsonView({ TaskJsonView.class })
	List<SubTask> subtasks;

	@ManyToMany
	@JoinTable(name = "tasks_tags", joinColumns = @JoinColumn(name = "taskid"), inverseJoinColumns = @JoinColumn(name = "tagid"))
	@JsonView({ TaskJsonView.class })
	List<Tag> tags;

	@OneToMany(mappedBy = "task")
	@JsonView({ TaskJsonView.class })
	List<TaskHistory> taskProperty;

	@OneToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name = "id")
	@JsonView({ TaskJsonView.class })
	TaskHistoryContent taskPropertyContent;

	public List<User> getUsersSubscribers() {
		return usersSubscribers;
	}

	public void setUsersSubscribers(List<User> usersSubscribers) {
		this.usersSubscribers = usersSubscribers;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setIsDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}

	public Priority getPriority() {
		return priority;
	}

	
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<User> getUsers() {
		return users;
	}

	// @SuppressWarnings("unchecked")
	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<SubTask> getSubtasks() {
		return subtasks;
	}

	public void setSubtasks(List<SubTask> subtasks) {
		this.subtasks = subtasks;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<TaskHistory> getTaskProperty() {
		return taskProperty;
	}

	public void setTaskProperty(List<TaskHistory> taskProperty) {
		this.taskProperty = taskProperty;
	}


	public TaskHistoryContent getTaskPropertyContent() {
		return taskPropertyContent;
	}

	public void setTaskPropertyContent(TaskHistoryContent taskPropertyContent) {
		this.taskPropertyContent = taskPropertyContent;
	}

	

}
