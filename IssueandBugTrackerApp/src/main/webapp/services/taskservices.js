tmsApp.service("taskServices", function($http) {

	this.searchTasks = function() {
		return $http.get("/all/task/getTaskId");
	}

	this.getHistory = function(taskId) {
		return $http.get('/all/task/audit/getaudit?task=' + taskId);
	}
})

tmsApp.service("commentServices", function($http) {

	this.addComment = function(projectId, taskId, title) {
		return $http.post('/all/task/add/Comment?project=' + projectId
				+ '&task=' + taskId, title);

	},

	this.editComment = function(projectId, taskId, commentId, message) {
		return $http.post('/all/task/edit/Comment/?project=' + projectId
				+ '&task=' + taskId + '&comment=' + commentId, message)

	},

	this.deleteComment = function(projectId, taskId, commentId) {
		return $http.post('/all/task/delete/Comment/?project=' + projectId
				+ '&task=' + taskId, commentId)

	}
});

tmsApp.service("subtaskServices", function($http) {
	this.addSubtask = function(projectId, taskId, subtasks) {
		return $http.post('/all/task/add/subtask?project=' + projectId
				+ '&task=' + taskId, subtasks)
	}

	this.deleteSubtask = function(projectId, taskId, subtaskId) {
		return $http.get('/all/task/delete/subtask?project=' + projectId
				+ '&task=' + taskId + '&subtask=' + subtaskId)
	}

	this.editSubtask = function(projectId, taskId, subtaskId, subtasks) {
		return $http.post('/all/task/edit/subtask?project=' + projectId
				+ '&task=' + taskId + '&subtask=' + subtaskId, subtasks)
	}

	this.changeSubtaskStatus = function(projectId, taskId, subTaskId, status) {
		return $http.get('/all/task/subtask?project=' + projectId + '&task='
				+ taskId + '&subtask=' + subTaskId + '&status=' + status)
	}
});

tmsApp.service("edittaskServices", function($http) {

	this.title = function(task) {
		return $http.post("/all/task/updatetask", task)
	}

	this.description = function(task) {
		return $http.post('/all/task/updatetask', task)
	}

	this.status = function(status) {
		return $http.post('/all/task/changestatus', status)
	}

	this.priority = function(priority) {
		return $http.post('/all/task/changepriority', priority)
	}

	this.startDate = function(date) {
		return $http.post('/all/task/editdate', date)
	}

	this.deadline = function(date) {
		return $http.post('/all/task/editdate', date)
	}

	this.users = function(users) {
		return $http.post('/all/task/edituser', users)
	}

	this.tag = function(tags) {
		return $http.post('/all/task/edittag', tags)
	}
})

tmsApp.service("fileServices", function($http) {

	this.uploadFile = function(FileUploader, $scope, url) {
		var uploader = $scope.uploader = new FileUploader({
			url : url,
		});
		// FILTERS
		uploader.filters.push({
			name : 'customFilter',
			fn : function(item, options) {
				return this.queue.length < 10;
			}
		})
	}

	this.getFiles = function(projectId, taskId) {
		return $http.get('/all/task/getfiles?project=' + projectId + '&task='
				+ taskId);
	}

	this.downloadFile = function(projectId, taskId, filename) {

		return $http({

			method : 'GET',
			url : '/all/task/download?project=' + projectId + '&task=' + taskId
					+ '&file=' + filename,
			responseType : 'blob',

		});
	}

	this.downloadAllFile = function(projectId, taskId, filename) {
		return $http.get('/all/task/download/all?project=' + projectId
				+ '&task=' + taskId + '&file=' + filename);

	}

	this.deleteFile = function(projectId, taskId, filename) {
		return $http.post('/all/task/fileupload/delete?project=' + projectId
				+ '&task=' + taskId, filename);
	}

})