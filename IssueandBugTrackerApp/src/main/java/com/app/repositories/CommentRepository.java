package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entities.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {

Comment findById(String id);
}
