
package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.Priority;


public interface PriorityRepository extends JpaRepository<Priority, String> {

}
