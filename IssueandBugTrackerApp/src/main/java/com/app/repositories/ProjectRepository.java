package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.entities.Project;
import com.app.entities.Tenant;

public interface ProjectRepository extends JpaRepository<Project, String> {
	
	@Query("select max(p.projectId) from Project p where p.tenant=:tenant")
	public long findByTenantMaxProjectId(@Param("tenant") Tenant tenant);

	List<Project> findByProjectNameAndTenant(String projectName, Tenant tenant);

	Project findByIdOrProjectNameAndTenant(String id, String name, Tenant tenant);

	List<Project> findByTenant(Tenant loggedInUsersTenant);

	Project findByProjectIdAndTenant(String id, Tenant tenant);

	/**
	 * @param projectId
	 * @param loggedInUsersTenant
	 * @return
	 */
	Project findByProjectIdAndTenant(long projectId,
			Tenant loggedInUsersTenant);

	

	

}
