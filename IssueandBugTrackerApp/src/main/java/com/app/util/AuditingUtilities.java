package com.app.util;
/**
 * @author Deepanjan
 */
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



@Service
public class AuditingUtilities {

	static Logger log = LoggerFactory.getLogger(AuditingUtilities.class);

	static String value;

	/**
	 * @param task
	 * @param taskAudit
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * 
	 *             Utility to get the attributes of an Entity which follows JPA
	 *             Convention
	 */
/*	@SuppressWarnings("rawtypes")
	public static String getAuditingProperty(Object objectProperty,
			Enum fieldProperty) throws IllegalArgumentException,
			IllegalAccessException {
		String newValue = null;
		for (Field field : objectProperty.getClass().getDeclaredFields()) {
			field.setAccessible(true); // You might want to set modifier to
										// public first.

			if (field.getName().toLowerCase()
					.equals(fieldProperty.toString().toLowerCase())) {
				Object value = field.get(objectProperty);
				if (value == (null)) {
					log.info("Field property " + field.getName() + " is null");
				} else {
					newValue = value.toString();
					break;
				}
			}
		}

		return newValue;
	}*/

	/*@SuppressWarnings("rawtypes")
	public static String getAuditingProperty(
			HashMap<Object, Object> objectProperty, Enum fieldProperty) {

		return null;
	}*/

	public static byte[] archieveTaskAuditContents(Object obj) throws IOException {
		ByteArrayOutputStream byteObjectOutputStream = new ByteArrayOutputStream();
		ObjectOutput objectOutputStream = new ObjectOutputStream(
				byteObjectOutputStream);
		objectOutputStream.writeObject(obj);

		byte[] auditPropertyContent = byteObjectOutputStream.toByteArray();
		return auditPropertyContent;
	}
}