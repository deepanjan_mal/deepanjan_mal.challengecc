tmsApp.controller('createprojectcontroller', function($scope, $http,
		$rootScope, tmsutility, toaster, ngToast, $timeout, $log) {
	tmsutility.method($scope);
	$scope.project = {};

	$scope.addProject = function() {
		var date = $('#startdate')[0].value;
		$scope.project.startDate = (new Date(date)).getTime();
		$http({
			method : 'POST',
			url : '/all/project/addproject',
			data : $scope.project
		}).success(function() {

			// success
			toaster.method("success", "Success!!", ngToast);
			$timeout(function() {
				location.href = "#/project";

			}, 500);

		}).error(function(err) {

			$log.info(err);
			// error
			toaster.method("danger", "Error!!", ngToast);

		})
	}

})