package com.app.project.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Project;
import com.app.entities.User;
import com.app.enums.Authority;
import com.app.project.services.ProjectService;
import com.app.repositories.ProjectRepository;
import com.app.util.AuthUtilService;
import com.app.util.ProjectAuthUtilities;
import com.app.util.views.ProjectJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class ProjectController {

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectService projectService;

	@Autowired
	ProjectAuthUtilities projectAuthUtilities;

	@Autowired
	AuthUtilService authUtilService;

	@RequestMapping("/all/project/addproject")
	@JsonView({ ProjectJsonView.class })
	public void addProject(@RequestBody Project project) throws ParseException {
		projectService.addProject(project);
	}

	@RequestMapping("/all/project/getprojects")
	@JsonView({ ProjectJsonView.class })
	public List<Project> getProjects(@RequestParam("value") String value) {
		return projectService.getProjects(value);
	}

	@RequestMapping("/all/project/getprojects/users")
	@JsonView({ ProjectJsonView.class })
	public List<Project> getUserProjects() {
		return projectService.getUserProjects();
	}

	@RequestMapping("/all/project/updateproject")
	@JsonView({ ProjectJsonView.class })
	public void updateProject(@RequestBody Project project) throws Exception {
		User user = authUtilService.getLoggedInUser();
		if (!user.getRoles().get(0).getRoleType().equals("admin")) {
			projectAuthUtilities.checkForAuthority(Authority.EDIT_PROJECT,
					project);
			projectService.updateProject(project);
		} else {
			projectService.updateProject(project);
 
		}
	}

	@RequestMapping("/all/project/getproject")
	@JsonView({ ProjectJsonView.class })
	public Project getUserProject(@RequestParam long projectId) {
		return projectService.getProject(projectId);
	}

}
