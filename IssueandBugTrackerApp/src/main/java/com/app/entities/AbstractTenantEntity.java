package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;

import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.app.util.AuthUtilService;
import com.app.util.InjectionHelper;
import com.app.util.TenantEntityListener;
import com.app.util.views.UserJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@MappedSuperclass
@EntityListeners({ TenantEntityListener.class })
public abstract class AbstractTenantEntity extends AbstractEntity implements
		Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "tenant_id")
	@JsonView({ UserJsonView.class })
	public Tenant tenant;

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	public AbstractTenantEntity(boolean shouldTenantBeInjected) {
		AuthUtilService authUtilService = InjectionHelper
				.getBean(AuthUtilService.class);
		this.tenant = authUtilService.getLoggedInUsersTenant();
	}

	protected AbstractTenantEntity() {

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
