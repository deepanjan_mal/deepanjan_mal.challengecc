package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.util.AbstractEntityListener;
import com.app.util.views.LoadUserJsonVew;
import com.app.util.views.MastersSubscribersJsonView;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.ProjectRoleJsonView;
import com.app.util.views.SubscribersJsonView;
import com.app.util.views.TagJsonView;
import com.app.util.views.TaskHistoryJsonView;
import com.app.util.views.TaskJsonView;
import com.app.util.views.UserJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.app.util.views.UserTenantJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@MappedSuperclass
@EntityListeners({ AbstractEntityListener.class, AuditingEntityListener.class })
public abstract class AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@JsonView({ UserJsonView.class, TaskJsonView.class, ProjectJsonView.class,
			MastersSubscribersJsonView.class, SubscribersJsonView.class,
			UserProjectRoleJsonView.class, LoadUserJsonVew.class,
			ProjectRoleJsonView.class, TaskJsonView.Summary.class,
			TagJsonView.class, TaskHistoryJsonView.class, UserProjectRoleJsonView.class,UserTenantJsonView.class })
	String id;

	@Column(name = "created_date")
	@CreatedDate
	@JsonView({ TaskJsonView.class, ProjectJsonView.class,
			TaskJsonView.Summary.class, TaskHistoryJsonView.class })
	public Date createdDate = new Date();

	@Column(name = "last_modified_date")
	@LastModifiedDate
	@JsonView({TaskJsonView.class, ProjectJsonView.class,
			TaskHistoryJsonView.class })
	public Date lastModifiedDate;

	@CreatedBy
	@ManyToOne
	@JoinColumn(name = "created_by_id")
	@JsonView({ TaskHistoryJsonView.class })
	public User createdBy;

	@LastModifiedBy
	@ManyToOne
	@JoinColumn(name = "last_modified_by_id")
	@JsonView({ TaskHistoryJsonView.class })
	public User lastModifiedBy;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	protected AbstractEntity() {

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

}
