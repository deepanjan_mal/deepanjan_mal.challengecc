package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.app.enums.Authority;
import com.app.util.views.ProjectRoleJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "project_authority")
public class ProjectAuthority implements Serializable {

	private static final long serialVersionUID = 8887645880218358673L;

	@Id
	@Column(name = "id")
	@JsonView({ ProjectRoleJsonView.class })
	private String id;

	@Column(name = "authority")
	@Enumerated(EnumType.STRING)
	@JsonView({ ProjectRoleJsonView.class })
	private Authority authority;

	@ManyToOne
	@JoinColumn(name="role_id")
	private ProjectRole role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public ProjectRole getRole() {
		return role;
	}

	public void setRole(ProjectRole role) {
		this.role = role;
	}

}
