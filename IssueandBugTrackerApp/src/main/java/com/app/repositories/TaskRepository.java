package com.app.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.entities.Project;
import com.app.entities.Task;
import com.app.entities.Tenant;
import com.app.entities.User;

public interface TaskRepository extends JpaRepository<Task, String> {
	Task findById(String object);

	@Query("select max(t.taskId) from Task t where t.tenant=:tenant")
	public int findByTenantMaxTaskId(@Param("tenant") Tenant tenant);

	List<Task> findByUsersAndProjectIdAndIsDelete(User loggedInUser, String id, boolean b);

	List<Task> findByUsersAndIsDelete(User loggedInUser, boolean b);

	List<Task> findByTitleIgnoreCaseContainingAndIsDelete(String taskName, boolean b);

	List<Task> findByTenant(Tenant loggedInUsersTenant);

	List<Task> findByStatusAndPriorityAndProjectIdAndIsDelete(String status, String priority, String projectid,
			boolean b);

	List<Task> findByPriorityAndProjectIdAndIsDelete(String priority, String id, boolean b);

	List<Task> findByStatusAndProjectIdAndIsDelete(String status, String projectId, boolean b);

	List<Task> findByUsersAndIsDeleteAndDeadline(User loggedInUser, boolean b, Date customdate);

	/**
	 * @param string
	 * @param loggedInUsersTenant
	 * @return
	 */
	Task findByTaskIdAndTenant(long id, Tenant loggedInUsersTenant);

	Task findByIdAndTenant(String taskid, Tenant loggedInUsersTenant);

	List<Task> findByProject(Project project);
	
	List<Task> findByUsers(User user);

}
