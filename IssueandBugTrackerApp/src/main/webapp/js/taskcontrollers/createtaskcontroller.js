tmsApp.controller('createtaskcontroller', function($scope, $rootScope, $http,
		createtask, tmsutility, ngToast, $log) {

	tmsutility.method($scope);
	if ($rootScope.projectfield) {
		$scope.save = function() {
			createtask.task($scope, $scope.task).success(function() {
				ngToast.create({
					className : 'success',
					content : 'Success!!'
				});

			}).error(function(err) {
				$log.info(err);
				ngToast.create({
					className : 'danger',
					content : 'Error!!'
				});
			})
		}
	} else {
		$scope.projectId = angular.copy($rootScope.projectId);
		$scope.task = {
			project : {
				"id" : $scope.projectId
			}
		}
		$scope.save = function() {
			createtask.task($scope, $scope.task).success(function() {
				ngToast.create({
					className : 'success',
					content : 'Success!!'
				});

			}).error(function(err) {
				$log.info(err);
				ngToast.create({
					className : 'danger',
					content : 'Error!!'
				});
			})
		}
	}

})