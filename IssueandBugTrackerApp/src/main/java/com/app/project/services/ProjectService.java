package com.app.project.services;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.entities.Project;
import com.app.repositories.ProjectRepository;
import com.app.repositories.ProjectRoleRepository;
import com.app.repositories.UserProjectRolesRepository;
import com.app.util.AuthUtilService;

/**
 * @author deepanjan
 *
 */

@Service
public class ProjectService {

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	ProjectRoleRepository projectRoleRepository;

	public Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Get list of all projects for a tenant
	 * 
	 * @param value
	 * @return List
	 */
	public List<Project> getProjects(String value) {
		if (value.equals("null")) {
			return projectRepository.findByTenant(authUtilService
					.getLoggedInUsersTenant());
		} else {
			return projectRepository.findByProjectNameAndTenant(value,
					authUtilService.getLoggedInUsersTenant());

		}
	}

	/**
	 * Get list of projects for the logged in user
	 * 
	 * @return List
	 */
	public List<Project> getUserProjects() {

		List<Project> project = userProjectRolesRepository
				.findByUser(authUtilService.getLoggedInUser());
		return project;
	}

	/**
	 * Update a particular project details in a tenant
	 * 
	 * @param project
	 */
	public void updateProject(Project project) {
		try {
			projectRepository.save(project);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add a new project to a tenant
	 * 
	 * @param project
	 * @throws ParseException
	 */
	public void addProject(Project project) throws ParseException {

		try {
			List<Project> projectExists = projectRepository
					.findByProjectNameAndTenant(project.getProjectName(),
							authUtilService.getLoggedInUsersTenant());
			if (!(projectExists.isEmpty())) {
				log.info("Project already Exists");
			} else {
				Project projectAdd = new Project();
				projectAdd.setId(UUID.randomUUID().toString());
				projectAdd.setProjectName(project.getProjectName());
				projectAdd.setDescription(project.getDescription());
				projectAdd.setStartDate(project.getStartDate());
				projectAdd.setTenant(authUtilService.getLoggedInUsersTenant());
				projectRepository.save(projectAdd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Get a particular project details
	 * 
	 * @param projectId
	 * @return Project
	 */
	public Project getProject(long projectId) {
		return projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
	}
}
