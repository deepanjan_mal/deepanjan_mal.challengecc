package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;

public class ProjectRoleJsonView {
	public interface Summary {
	};

	public interface ProjectRoleReadView extends PaginatedResult {
	};
}
