package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "task_audit_contents")
public class TaskHistoryContent extends AbstractEntity implements Serializable{

	private static final long serialVersionUID = -631739872365789611L;

	@Lob
	@Column(name = "task_property")
	private	byte[] taskProperty;

	@OneToOne
	public Task task;

	public byte[] getTaskProperty() {
		return taskProperty;
	}

	public void setTaskProperty(byte[] bs) {
		this.taskProperty = bs;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
