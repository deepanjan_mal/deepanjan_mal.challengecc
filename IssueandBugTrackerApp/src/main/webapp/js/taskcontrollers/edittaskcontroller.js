tmsApp
		.controller(
				'edittaskcontroller',
				[
						'$scope',
						'$rootScope',
						'$http',
						'$location',
						'tmsutility',
						'$routeParams',
						'FileUploader',
						'$q',
						'$timeout',
						'getusersservice',
						'CommentService',

						'toaster',
						'ngToast',
						'getClass',
						'$log',
						'getGlyphicons',
						'authorityutility',
						'taskFactory',
						function($scope, $rootScope, $http, $location,
								tmsutility, $routeParams, FileUploader, $q,
								$timeout, getusersservice, CommentService,
								toaster, ngToast, getClass, $log,
								getGlyphicons, authorityutility, taskFactory) {

							var task = new taskFactory();

							authorityutility.method($http,
									$routeParams.projectId).success(
									function(res) {
										$scope.authorities = res;
									})

							$scope.searchTasks = function(term) {
								task.searchTasks().success(function(response) {
									$scope.taskIds = [];

									angular.forEach(response, function(taskId) {
										if (taskId != $routeParams.taskId) {
											$scope.taskIds.push({
												label : taskId
											})
										}
									})

								}).error(function(err) {
									$log.info(err);
								})
							};

							$scope.baseUrl = "#/project/"
									+ $routeParams.projectId + "/task/";

							$scope.parseComment = function parseComment(comment) {
								return CommentService.parseComment(comment,
										$scope.baseUrl);

							};

							$scope.addComment = function() {
								task.addComment($routeParams.projectId,
										$routeParams.taskId, $scope.title)
										.success(
												function(response) {
													$scope.title = "";
													toaster.method("success",
															"Success!!",
															ngToast);
													$scope.getTask();
												}).error(function(err) {
											$log.info(err);
										})

							};

							$scope.deleteComment = function(commentId) {

								task
										.deleteComment($routeParams.projectId,
												$routeParams.taskId, commentId)
										.success(
												function(response) {
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function(err) {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})
							}
							$scope.closeCommentEditView = function() {
								$scope.selected = {}
							}
							$scope.selected = {}
							$scope.getTemplate = function(comment) {

								if ($scope.selected.id === comment.id) {
									return "edit";
								} else {
									return "view";
								}
							};

							$scope.edit = function(comment) {

								$scope.selected = angular.copy(comment);
							}

							$scope.editComment = function(commentId, message) {
								task
										.editComment($routeParams.projectId,
												$routeParams.taskId, commentId,
												message)
										.success(
												function() {
													$scope.selected = {}
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function() {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})

								$scope.editable = null;

							};

							$scope.loadUsers = function($query) {
								return $http
										.get(
												'all/loadusers?query='
														+ $query
														+ '&projectId='
														+ $routeParams.projectId,
												{
													cache : true
												})
										.then(
												function(response) {

													var users = response.data;

													return users
															.filter(function(
																	user) {
																return user.name
																		.toLowerCase()
																		.indexOf(
																				$query
																						.toLowerCase()) != -1;
															});

												});
							};

							$scope.addUsers = function() {

								$scope.task = {
									id : $routeParams.taskId,
									users : $scope.users
								}

								task.addUsers($scope.task).success(
										function() {
											$scope.getTask();
											toaster.method("success",
													"Success!!", ngToast);
										}).error(
										function(err) {
											$log.info(err);
											toaster.method("danger", "Error!!",
													ngToast);
										})

							}

							$scope.getTask = function() {

								$http
										.get(
												'/all/task/gettask?taskId='
														+ $routeParams.taskId)
										.success(
												function(response) {

													if (response.isDelete === false) {
														$scope.editTask = true;
														$scope.getHistory();
														$scope.editedTask = response
														$scope.users = response.users;

														$scope.statusGlyphicon = getGlyphicons
																.status($scope.editedTask.status).glyphicon

														$scope.priorityglyphicon = getGlyphicons
																.priority($scope.editedTask.priority).glyphicon;

														$scope.getFiles();
													} else {
														$scope.showEditTask = true;
													}
												}).error(function(err) {
											$log.info(err);
										})
							}

							$scope.holdCommentId = function(id) {

								$rootScope.commentuploadurl = '/all/task/api/fileupload/comment?project='
										+ $routeParams.projectId
										+ '&task='
										+ $routeParams.taskId
										+ '&comment='
										+ id;
								task.uploadFile(FileUploader, $scope,
										$rootScope.commentuploadurl);
							};

							$scope.taskuploadurl = '/all/task/api/fileupload?project='
									+ $routeParams.projectId
									+ '&task='
									+ $routeParams.taskId;

							task.uploadFile(FileUploader, $scope,
									$scope.taskuploadurl);
							tmsutility.method($scope);
							$scope.getTask();

							// Getting list of files for tasks
							$scope.getFiles = function() {
								task
										.getFiles($routeParams.projectId,
												$routeParams.taskId)
										.success(
												function(response) {

													$scope.fileName = [];
													angular
															.forEach(
																	response,
																	function(
																			filename) {
																		$scope.fileName
																				.push({
																					name : filename
																				})
																	})
													$scope.files = $scope.fileName;
													$http
															.get(
																	'/all/task/gettask/?taskId='
																			+ $routeParams.taskId)
															.success(
																	function(
																			response) {

																		$scope.commentList = response.comments;
																		angular
																				.forEach(
																						$scope.commentList,
																						function(
																								list) {

																							$http
																									.get(
																											'/all/task/getfiles/comment?project='
																													+ $routeParams.projectId
																													+ '&task='
																													+ $routeParams.taskId
																													+ '&comment='
																													+ list.id)
																									.success(
																											function(
																													response) {
																												$scope.commentfileName = [];
																												angular
																														.forEach(
																																response,
																																function(
																																		filename) {
																																	$scope.commentfileName
																																			.push({
																																				name : filename
																																			})
																																})
																												$scope.commentfiles = $scope.commentfileName;

																											})
																						})

																	});
												}).error(function(err) {
											$log.info(err);
										})
							}
							$scope.getFiles();

							// downloading a file
							$scope.downloadFile = function(filename) {

								task
										.downloadFile($routeParams.projectId,
												$routeParams.taskId, filename)
										.success(
												function(data) {
													var blob = new Blob(
															[ data ],
															{
																type : 'application/octet-stream'
															});
													saveAs(blob, filename);
												}).error(function(err) {
											$log.info(err);
										});
							};

							// downloading all file
							$scope.downloadAllFile = function(filename) {

								task
										.downloadAllFile(
												$routeParams.projectId,
												$routeParams.taskId, filename)
										.success(
												function(data) {

													task
															.downloadFile(
																	$routeParams.projectId,
																	$routeParams.taskId,
																	filename)
															.success(
																	function(
																			data) {
																		var blob = new Blob(
																				[ data ],
																				{
																					type : 'application/octet-stream'
																				});
																		saveAs(
																				blob,
																				filename);
																		$rootScope.isDeleteZipFile = "true";
																		$scope
																				.deleteFile(filename);
																	})
															.error(
																	function(
																			err) {
																		$log
																				.info(err);
																	});
												}).error(function(err) {
											$log.info(err);
										});
							}

							$rootScope.isDeleteZipFile == "false";
							// deleting a file
							$scope.deleteFile = function(file) {

								$scope.fileName = {
									"filename" : file
								};
								task
										.deleteFile($routeParams.projectId,
												$routeParams.taskId,
												$scope.fileName)
										.success(
												function(response) {
													$scope.getTask();
													$scope.getFiles();
													if (!($rootScope.isDeleteZipFile))
														toaster.method(
																"success",
																"Success!!",
																ngToast);

												}).error(function(err) {
											$log.info(err);
										})
							}

							$scope.updateTitle = function() {

								$scope.editTitle = {
									id : $scope.editedTask.id,
									title : $scope.editedTask.title,
								}
								task.editTitle($scope.editTitle).success(

										function() {
											$scope.getTask();
											toaster.method("success",
													"Success!!", ngToast);
										}).error(
										function(err) {
											$log.info(err);
											toaster.method("danger", "Error!!",
													ngToast);
										})

							};

							$scope.tagText = undefined

							$http.get(
									'/all/task/project/gettags?projectId='
											+ $routeParams.projectId).success(
									function(response) {
										$scope.tags = [];
										angular.forEach(response, function(
												value) {
											$scope.tags.push(value.tagName);
										})

									}).error(function(error) {
								$log.info(error);
							})

							$scope.addTags = function() {

								$scope.taskk = {
									taskId : $routeParams.taskId,
									tags : $scope.editedTask.tags,
									project : {
										projectId : $routeParams.projectId
									}

								}

								task.addTags($scope.taskk).success(
										function() {
											$scope.getTask();
											// success
											toaster.method("success",
													"Success!!", ngToast);

										}).error(
										function(err) {
											$log.info(err);
											// error
											toaster.method("danger", "Error!!",
													ngToast);
										})

							};

							$scope.changeStatus = function(status, glyphicon) {

								$scope.statusglyphicon = glyphicon;
								$scope.editStatus = {
									id : $scope.editedTask.id,
									status : status
								}
								task
										.changeStatus($scope.editStatus)
										.success(
												function() {
													$scope.getTask();
													// success
													ngToast
															.create({
																className : 'success',
																content : 'Well Done! Status Changed Successfully'
															});

												})
										.error(
												function(err) {
													$log.info(err);
													// error
													ngToast
															.create({
																className : 'danger',
																content : 'Oh snap! Try submitting again'
															});

												})
							};

							$scope.changePriority = function(priority,
									glyphicon) {

								$scope.priorityglyphicon = glyphicon;
								$scope.editPriority = {
									id : $scope.editedTask.id,
									priority : priority
								}
								task
										.changePriority($scope.editPriority)
										.success(
												function() {
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function(err) {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})
							};
							$scope.editStartDate = function() {
								var startDate = $('#startdate')[0].value;
								$scope.editedTask.startDate = (new Date(
										startDate)).getTime();
								$scope.editDate = {
									id : $scope.editedTask.id,
									startDate : $scope.editedTask.startDate,
								}
								task
										.changestartDate($scope.editDate)
										.success(
												function() {
													$scope.getTask();
													// success
													ngToast
															.create({
																className : 'success',
																content : 'Well Done! Schedule Changed Successfully'
															});

												})
										.error(
												function(err) {
													$log.info(err);
													// error
													ngToast
															.create({
																className : 'danger',
																content : 'Oh snap! Try submitting again'
															});

												})
							};

							$scope.editDeadline = function() {
								var deadLine = $('#deadline')[0].value;
								$scope.editedTask.deadline = (new Date(deadLine))
										.getTime();
								$scope.editedDate = {
									id : $scope.editedTask.id,
									deadLine : $scope.editedTask.deadline

								}
								task
										.changeDeadline($scope.editedDate)

										.success(
												function() {
													$scope.getTask();
													// success
													ngToast
															.create({
																className : 'success',
																content : 'Well Done! Deadline Changed Successfully'
															});

												})
										.error(
												function(err) {
													$log.info(err);
													// error
													ngToast
															.create({
																className : 'danger',
																content : 'Oh snap! Try submitting again'
															});

												})
							};

							$scope.editDescription = function() {

								$scope.editdescription = {
									id : $scope.editedTask.id,
									description : $scope.editedTask.description
								}

								task
										.editDescription($scope.editdescription)
										.success(
												function() {
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function(err) {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})

							};

							// Subtasks
							// Creating new subtask
							$scope.createNewSubtask = function() {
								task
										.addSubtask($routeParams.projectId,
												$routeParams.taskId,
												$scope.subtasks)
										.success(
												function() {
													$scope.subtasks.message = "";
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function(err) {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})

							}

							// Editing Subtask
							$scope.editSubtask = function(subtaskId, value) {
								$scope.subtasks = {
									"message" : value
								};

								task.editSubtask($routeParams.projectId,
										$routeParams.taskId, subtaskId,
										$scope.subtasks).success(
										function() {
											$scope.subtasks.message = "";
											$scope.getTask();
											toaster.method("success",
													"Success!!", ngToast);
										}).error(
										function(err) {
											$log.info(err);
											toaster.method("danger", "Error!!",
													ngToast);
										})
							}

							// Change Subtask Status
							$scope.changeSubtaskStatus = function(subTaskId,
									event) {

								if (event.currentTarget.checked) {
									$scope.status = true;
								} else {
									$scope.status = false;
								}

								task
										.changeSubtaskStatus(
												$routeParams.projectId,
												$routeParams.taskId, subTaskId,
												$scope.status)
										.success(
												function(response) {
													$scope.getTask();

													ngToast
															.create({
																className : 'success',
																content : 'Well done! Status changed successfully'
															});
												})
										.error(
												function(err) {
													$log.info(err);
													ngToast
															.create({
																className : 'danger',
																content : 'Oh snap! Try submitting again'
															});
												})
							}

							// Delete Subtask
							$scope.deleteSubtask = function(subtaskId) {
								task
										.deleteSubtask($routeParams.projectId,
												$routeParams.taskId, subtaskId)
										.success(
												function() {
													$scope.getTask();
													toaster.method("success",
															"Success!!",
															ngToast);
												})
										.error(
												function(err) {
													$log.info(err);
													toaster.method("danger",
															"Error!!", ngToast);
												})
							}

							$('.title').keypress(function(e) {
								if (e.keyCode == 13)
									return false
							})

							// Task Auditing function
							$scope.getHistory = function() {
								task
										.getHistory($routeParams.taskId)
										.success(
												function(response) {
													$scope.taskHistory = [];
													angular
															.forEach(
																	response,
																	function(
																			message) {
																		$scope.classIcon = getClass
																				.getclass(message);

																		$scope.taskHistory
																				.push({
																					"message" : message,
																					"className" : $scope.classIcon
																				});
																	});
													$scope.history = $scope.taskHistory;
												}).error(function(err) {
											$log.info(err);
										})
							}
							$scope.getHistory();

							$scope.loadTags = function($query) {
								return $http
										.get(
												'/all/task/gettags?searchletter='
														+ $query
														+ '&projectId='
														+ $routeParams.projectId,
												{
													cache : true
												})
										.then(
												function(response) {

													var tags = response.data
													return tags
															.filter(function(
																	tag) {
																return tag.tagName
																		.toLowerCase()
																		.indexOf(
																				$query
																						.toLowerCase()) != -1;
															});

												})

							}
						} ])