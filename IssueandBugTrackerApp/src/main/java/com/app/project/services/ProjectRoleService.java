package com.app.project.services;
/**
 * @author Deepanjan
 */
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.entities.Project;
import com.app.entities.ProjectAuthority;
import com.app.entities.ProjectRole;
import com.app.entities.Tenant;
import com.app.entities.User;
import com.app.entities.UserProjectRole;
import com.app.enums.Authority;
import com.app.enums.TenantRole;
import com.app.repositories.ProjectAuthorityRepository;
import com.app.repositories.ProjectRepository;
import com.app.repositories.ProjectRoleRepository;
import com.app.repositories.UserProjectRolesRepository;
import com.app.repositories.UserRepository;
import com.app.util.AuthUtilService;

@Service
public class ProjectRoleService {

	@Autowired
	ProjectRoleRepository projectRoleRepository;

	@Autowired
	ProjectAuthorityRepository projectAuthorityRepository;

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	UserRepository userRepository;

	public Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Add new project role and project authorities specific to a tenant
	 * 
	 * @param projectRole
	 * @return void
	 */

	public void addRoleAndAuthorities(ProjectRole projectRole) {
		int index = 0;
		projectRole.setTenant(authUtilService.getLoggedInUsersTenant());
		projectRoleRepository.save(projectRole);
		if (projectRole.getAuthorities() != null) {
			List<ProjectAuthority> authorities = projectRole.getAuthorities();
			for (ProjectAuthority projectAuthority : authorities) {
				projectAuthority.setAuthority(projectRole.getAuthorities()
						.get(index).getAuthority());
				projectAuthority.setRole(projectRole);
				projectAuthority.setId(UUID.randomUUID().toString());
				projectAuthorityRepository.save(projectAuthority);
				index++;
			}
		}
	}

	/**
	 * Add users to a project with particular project role which is unique
	 * across a project
	 * 
	 * @param userProjectRole
	 * @return void
	 */
	public void addUserProjectRoles(UserProjectRole userProjectRole) {
		Project project = projectRepository.findByProjectIdAndTenant(
				userProjectRole.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		userProjectRole.setProject(project);
		if (userProjectRole.getUser() != null)
			userProjectRolesRepository.save(userProjectRole);
		else
			throw new NullPointerException(
					"No user present in userProjectRole...");

	}

	/**
	 * Delete users from a project with particular project role which is unique
	 * across a project
	 * 
	 * @param userProjectRole
	 * @throws Exception
	 * @return void
	 */
	public void deleteUserProjectRoles(UserProjectRole userProjectRole)
			throws Exception {
		UserProjectRole removeUserProjectRole = userProjectRolesRepository
				.findById(userProjectRole.getId());
		userProjectRolesRepository.delete(removeUserProjectRole);
	}

	/**
	 * Update the role for a particular user specific to the project
	 * 
	 * @param userProjectRole
	 * @throws ParseException
	 * @return void
	 */
	public void updateUserProjectRoles(UserProjectRole userProjectRole)
			throws ParseException {
		Project project = projectRepository.findByProjectIdAndTenant(
				userProjectRole.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		userProjectRole.setProject(project);
		UserProjectRole userProjectRoleUpdate = new UserProjectRole();
		userProjectRoleUpdate = userProjectRolesRepository
				.findByProjectAndUser(userProjectRole.getProject(),
						userProjectRole.getUser());
		userProjectRoleUpdate.setProjectRole(userProjectRole.getProjectRole());
		userProjectRolesRepository.save(userProjectRoleUpdate);

	}

	/**
	 * Get a project roles specific to tenant
	 * 
	 * @return List
	 */
	public List<ProjectRole> getProjectRoles() {
		return projectRoleRepository.findByTenant(authUtilService
				.getLoggedInUsersTenant());
	}

	/**
	 * Get list of all members in a project along with there roles
	 * 
	 * @param id
	 * @return List
	 */
	public List<UserProjectRole> getUserProjectRoles(long id) {
		String projectId = projectRepository.findByProjectIdAndTenant(id,
				authUtilService.getLoggedInUsersTenant()).getId();
		return userProjectRolesRepository.findByProjectId(projectId);
	}

	/**
	 * Get Project Roles and Authorities specific to a tenant
	 * 
	 * @return List
	 */
	public List<ProjectRole> getProjectRolesAndAuthorities() {
		return projectRoleRepository.findByTenant(authUtilService
				.getLoggedInUsersTenant());
	}

	/**
	 * Get the predefined authorities declared in the Authority enum for
	 * populating in the application
	 * 
	 * @return HashMap
	 */
	public HashMap<Authority, String> getAuthorities() {
		HashMap<Authority, String> listOfAuthorities = new HashMap<Authority, String>();
		for (Authority authority : Authority.values()) {
			listOfAuthorities.put(authority, authority.value());
		}
		return listOfAuthorities;
	}

	/**
	 * Update tenant specific roles and authorities
	 * 
	 * @param role
	 */
	public void updateRolesAndAthorities(ProjectRole role) {
		int i = 0;
		ProjectRole projectRole = projectRoleRepository.findById(role.getId());
		List<ProjectAuthority> projectAuthorities = projectAuthorityRepository
				.findByRoleId(role.getId());
		if (!role.getAuthorities().isEmpty()) {
			projectAuthorityRepository.delete(projectAuthorities);
			for (ProjectAuthority a : role.getAuthorities()) {
				a.setId(UUID.randomUUID().toString());
				a.setAuthority(role.getAuthorities().get(i).getAuthority());
				a.setRole(role);
				i++;
				projectAuthorityRepository.save(a);

			}
		}
		projectRole.setRole(role.getRole());
		projectRoleRepository.save(projectRole);
	}

	/**
	 * add default tenant roles while adding a new tenant
	 * 
	 * @param tenant
	 */
	public void addDefaultTenantRole(Tenant tenant) {
		ProjectRole projectRole = new ProjectRole();

		for (TenantRole tenantRole : TenantRole.values()) {
			projectRole.setId(UUID.randomUUID().toString());
			projectRole.setRole(tenantRole.toString());
			projectRole.setTenant(tenant);
			projectRoleRepository.save(projectRole);
			addDefaultAuthority(projectRole);
		}
	}

	/**
	 * add default tenant roles while adding a new tenant
	 * 
	 * @param projectRole
	 */
	public void addDefaultAuthority(ProjectRole projectRole) {
		for (Authority authority : Authority.values()) {
			ProjectAuthority projectAuthority = new ProjectAuthority();
			projectAuthority.setAuthority(authority);
			projectAuthority.setId(UUID.randomUUID().toString());
			projectAuthority.setRole(projectRole);
			projectAuthorityRepository.save(projectAuthority);
		}
	}

	/**
	 * @param projectId
	 * @return
	 */
	public HashMap<Authority, Boolean> getProjectRoleAuthorities(long projectId) {
		String id = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant()).getId();
		List<UserProjectRole> authorities = userProjectRolesRepository
				.findByProjectIdAndUserId(id, authUtilService.getLoggedInUser()
						.getId());
		HashMap<Authority, Boolean> projectRoleAuthorities = new HashMap<Authority, Boolean>();
		if (!(authorities.isEmpty())) {
			Iterator<ProjectAuthority> itr = authorities.get(0)
					.getProjectRole().getAuthorities().iterator();

			while (itr.hasNext()) {

				projectRoleAuthorities.put(itr.next().getAuthority(), true);
			}
		}
		return projectRoleAuthorities;
	}

	/**
	 * fetch users for a project
	 * 
	 * @param projectId
	 * @return
	 * 
	 */
	public List<User> getUserForProject(long projectId) {

		String id = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant()).getId();
		List<User> users = userRepository.findByTenant(authUtilService
				.getLoggedInUsersTenant());
		List<User> userProject = new ArrayList<User>();
		for (User user : users) {
			if (!user.getUsername().equals("super")) {
				UserProjectRole userprojectrole = userProjectRolesRepository
						.findByProjectAndUser(projectRepository.findOne(id),
								user);
				if (userprojectrole == null
						&& projectRepository.findOne(id) != null) {
					userProject.add(user);
				}
			}
		}
		return userProject;
	}
}
