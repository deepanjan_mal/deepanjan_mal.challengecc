package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;


public class UserRoleJsonView {

	public interface UserRoleSumaryView {
	};

	public interface UserRoleReadView extends PaginatedResult {
	};

}
