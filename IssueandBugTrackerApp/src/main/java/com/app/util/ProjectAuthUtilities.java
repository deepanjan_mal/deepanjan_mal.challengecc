package com.app.util;
/**
 * @author Deepanjan
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.entities.Project;
import com.app.entities.User;
import com.app.entities.UserProjectRole;
import com.app.enums.Authority;
import com.app.repositories.UserProjectRolesRepository;

@Service
public class ProjectAuthUtilities {

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	public void checkForAuthority(Authority authority, Project project)
			throws Exception {
		User user = authUtilService.getLoggedInUser();
		// if (!user.getRoles().get(0).getRoleType().equals("admin")) {
		UserProjectRole userProjectRole = userProjectRolesRepository
				.findByProjectAndUser(project, user);
		if (!userProjectRole.getProjectRole().containsAuthority(authority)) {
			throw new RuntimeException("Action is not valid for user"
					+ authority + " : " + user);
			// }
		}
	}
}
