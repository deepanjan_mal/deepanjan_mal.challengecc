package com.app.enums;

import java.util.Date;

import org.apache.commons.lang3.text.StrBuilder;

import com.app.entities.User;
import com.app.entities.audit.TaskHistory;
import com.app.util.DateUtilities;

/**
 * @author Deepanjan
 */
public enum TaskAudit {

	TITLE("Title was changed #oldVal to #newVal by #user #date"),

	STATUS("Status was changed to #newVal by #user #date"),

	PRIORITY("Priority was changed to #newVal by #user #date"),

	DESCRIPTION("Description was changed #oldVal to #newVal by #user #date"),

	STARTDATE("Start date was changed #oldVal to #newVal by #user #date"),

	COMPLETEDATE("Complete date was changed #oldVal to #newVal by #user #date"),

	DEADLINE("Deadline was changed #oldVal to #newVal by #user #date"),

	SUBTASK("Subtasks was changed #oldVal to #newVal by #user #date"),

	COMMENT("Comments was changed #oldVal to #newVal by #user #date"),

	TAG("Tags was changed #oldVal to #newVal by #user #date"),

	FILE("Files added #oldVal to #newVal by #user #date"),
	
	USER("Issue was assigned to #newVal by #user #date");

	private TaskAudit(String template) {
		this.template = template;
	}

	public String getAuditMessage(TaskHistory property) {
		if (property.getNewValue() != null || property.getOldValue() != null) {
			return getAuditMessage(property.getOldValue(),
					property.getNewValue(), property.getCreatedBy(),
					property.getCreatedDate());
		} else {
			return null;
		}
	}

	public String getAuditMessage(String oldVal, String newVal, User user,
			Date date) {
		return new StrBuilder(template).replaceAll("#oldVal", oldVal)
				.replaceAll("#newVal", newVal)
				.replaceAll("#user", user.getUsername())
				.replaceAll("#date", DateUtilities.generateAuditDate(date)).toString();
	}

	private String template;

}
