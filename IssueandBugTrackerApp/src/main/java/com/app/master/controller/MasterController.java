package com.app.master.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Tenant;
import com.app.entities.User;
import com.app.master.services.MasterService;
import com.app.util.views.TenantConfigurationView;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class MasterController {

	@Autowired
	MasterService masterControllerService;

	@RequestMapping("/sa/add/tenant/user")
	public HashMap<String, Object> addTenant(@RequestBody HashMap<String, Object> map) throws Exception {
		ObjectMapper objMap = new ObjectMapper();
		Tenant tenant = objMap.convertValue(map.get("tenant"), Tenant.class);
		User user =objMap.convertValue(map.get("user"), User.class);
		return masterControllerService.addTenant(tenant, user);
	}

	@RequestMapping("/sa/gettenant")
	public List<Tenant> getTenant(@RequestParam String tenantname) {
		return masterControllerService.getTenant(tenantname);
	}

	@RequestMapping("/sa/listtenant")
	@JsonView(TenantConfigurationView.class)
	public List<Tenant> getListOfTenant() {
		return masterControllerService.getListOfTenants();
	}

}
