/**
 * 
 */
package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.TaskHistoryContent;

public interface TaskPropertyContentRepository extends JpaRepository<TaskHistoryContent, String> {

}
