package com.app.util;

import java.util.List;

import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.app.entities.Task;
import com.app.repositories.TaskRepository;

/**
 * @author Deepanjan
 */
public class TaskEntityListener {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	TaskRepository taskRepository;

	@PrePersist
	public void setTaskId(Task task) throws Exception{

		InjectionHelper.autowire(this, this.authUtilService);
		List<Task> taskExists = taskRepository.findByTenant(authUtilService
				.getLoggedInUsersTenant());
		if (!taskExists.isEmpty()) {
			int maxId = taskRepository.findByTenantMaxTaskId(authUtilService
					.getLoggedInUsersTenant());
			task.setTaskId((maxId) + 1);
		} else {
			task.setTaskId(1);
		}

	}

}
