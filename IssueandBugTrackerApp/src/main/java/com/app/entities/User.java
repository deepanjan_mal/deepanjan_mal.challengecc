package com.app.entities;
/**
 * @author Deepanjan
 */
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.userdetails.UserDetails;

import com.app.enums.UserEnable;
import com.app.util.views.LoadUserJsonVew;
import com.app.util.views.MastersSubscribersJsonView;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.SubscribersJsonView;
import com.app.util.views.TaskJsonView;
import com.app.util.views.UserJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.app.util.views.UserTenantJsonView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"tenant_id", "username" }),@UniqueConstraint(columnNames = {
				"tenant_id", "email" }) })
public class User extends AbstractTenantEntity implements UserDetails {

	private static final long serialVersionUID = 7217841877446097601L;

	@Column(name = "name")
	@JsonView({ UserJsonView.class, ProjectJsonView.class, TaskJsonView.class,
			LoadUserJsonVew.class, MastersSubscribersJsonView.class,
			SubscribersJsonView.class })
	public String name;

	@Column(name = "username")
	@JsonView({ UserJsonView.class, ProjectJsonView.class, TaskJsonView.class,
			UserProjectRoleJsonView.class,UserTenantJsonView.class })
	public String username;

	@Column(name = "email")
	@JsonView({ UserJsonView.class,LoadUserJsonVew.class })
	public String email;

	@Column(name = "password")
	public String password;

	@Column(name = "enabled")
	private boolean enabled = false;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	@JsonView({ UserJsonView.class, TaskJsonView.class })
	private UserEnable status;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonView(UserJsonView.class)
	public List<UserRole> roles;

	@ManyToMany(mappedBy = "users")
	List<Task> tasks;

	@ManyToMany(mappedBy = "usersSubscribers")
	List<Task> tasksSubscribers;

	public List<Task> getTasksSubscribers() {
		return tasksSubscribers;
	}

	public void setTasksSubscribers(List<Task> tasksSubscribers) {
		this.tasksSubscribers = tasksSubscribers;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public UserEnable getStatus() {
		return status;
	}

	public void setStatus(UserEnable status) {
		this.status = status;
	}

	@JsonIgnore
	public boolean isSuper() {
		/*
		 * List<UserRoles> authorities = (List<UserRoles>)
		 * this.getAuthorities(); if (authorities != null) { for (UserRoles
		 * userRole : authorities) { if
		 * (userRole.getAuthority().equals("SUPER")) { return true; } } }
		 */
		return false;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public Collection<UserRole> getAuthorities() {
		return this.roles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((tasks == null) ? 0 : tasks.hashCode());
		result = prime
				* result
				+ ((tasksSubscribers == null) ? 0 : tasksSubscribers.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (roles == null) {
			if (other.roles != null) {
				return false;
			}
		} else if (!roles.equals(other.roles)) {
			return false;
		}
		if (tasks == null) {
			if (other.tasks != null) {
				return false;
			}
		} else if (!tasks.equals(other.tasks)) {
			return false;
		}
		if (tasksSubscribers == null) {
			if (other.tasksSubscribers != null) {
				return false;
			}
		} else if (!tasksSubscribers.equals(other.tasksSubscribers)) {
			return false;
		}
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}

}
