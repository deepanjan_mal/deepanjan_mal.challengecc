package com.app.task.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.entities.Project;
import com.app.entities.SubTask;
import com.app.entities.Tag;
import com.app.entities.Task;
import com.app.enums.Authority;
import com.app.enums.Priority;
import com.app.enums.Status;
import com.app.repositories.ProjectRepository;
import com.app.repositories.TaskRepository;
import com.app.repositories.UserProjectRolesRepository;
import com.app.task.services.TaskAuditService;
import com.app.task.services.TaskService;
import com.app.util.AuthUtilService;
import com.app.util.ProjectAuthUtilities;
import com.app.util.views.TagJsonView;
import com.app.util.views.TaskHistoryJsonView;
import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author deepanjan
 */
@RestController
@RequestMapping("/all/task")
public class TaskController {

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	TaskService taskService;

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	ProjectAuthUtilities projectAuthUtilities;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	@Autowired
	TaskAuditService taskAuditService;

	public Logger log = LoggerFactory.getLogger(getClass());

	@RequestMapping("/addtask")
	@JsonView({ TaskJsonView.class })
	public Map<String, Object> addTask(@RequestBody Task task) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(task.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());

		projectAuthUtilities.checkForAuthority(Authority.CREATE_ISSUE, project);
		return taskService.addTask(task, project);

	}

	@RequestMapping("/searchtasks")
	@JsonView({ TaskJsonView.Summary.class })
	public List<Task> searchTasks(@RequestParam(value = "value") String value) throws ParseException {

		return taskService.searchTasks(value);

	}

	@RequestMapping("/gettasks")
	@JsonView({ TaskJsonView.Summary.class })
	public List<Task> getTasks(@RequestParam(value = "status") List<Status> status,
			@RequestParam(value = "priority") List<Priority> priority,
			@RequestParam(value = "projectId") long projectId, @RequestParam(value = "deadline") String deadline,
			@RequestBody List<Tag> filterTags) {
		List<String> tagids = new ArrayList<String>();
		for (Tag tag : filterTags) {
			tagids.add(tag.getId());
		}
		return taskService.getTasks(status, priority, projectId, deadline, tagids);
	}

	@RequestMapping("/getmytasks")
	@JsonView({ TaskJsonView.Summary.class })
	public List<Task> getMyTasks(@RequestParam(value = "status") List<Status> status,
			@RequestParam(value = "priority") List<Priority> priority,
			@RequestParam(value = "projectId") long projectId, @RequestParam(value = "deadline") String deadline,
			@RequestParam(value = "userid") String userid, @RequestBody List<Tag> filterTags) {
		List<String> tagids = new ArrayList<String>();
		for (Tag tag : filterTags) {
			tagids.add(tag.getId());
		}
		return taskService.getMyTasks(status, priority, projectId, deadline, userid);
	}

	@RequestMapping("/gettask")
	@JsonView({ TaskJsonView.class })
	public Task getTask(@RequestParam("taskId") Long taskId) {
		return taskService.getTask(taskId);
	}

	@RequestMapping("/updatetask")
	@JsonView({ TaskJsonView.class })
	public void updateTask(@RequestBody Map<String, Object> task) throws Exception {
		Task projecttask = taskRepository.findById(task.get("id").toString());
		Project project = projectRepository.findByProjectIdAndTenant(projecttask.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.updateTask(task);
	}

	@RequestMapping("/changestatus")
	@JsonView({ TaskJsonView.class })
	public void changeStatus(@RequestBody Task taskstatus) throws Exception {
		Task task = taskRepository.findById(taskstatus.getId());
		Project project = projectRepository.findByProjectIdAndTenant(task.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		if (taskstatus.getStatus().toString().equalsIgnoreCase(Status.CLOSE.toString())) {
			projectAuthUtilities.checkForAuthority(Authority.CLOSE_ISSUE, project);
			taskService.changeStatus(task, taskstatus);
		} else {
			projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
			taskService.changeStatus(task, taskstatus);
		}
	}

	@RequestMapping("/changepriority")
	@JsonView({ TaskJsonView.class })
	public void changePriority(@RequestBody Task taskpriority) throws Exception {
		Task task = taskRepository.findById(taskpriority.getId());
		Project project = projectRepository.findByProjectIdAndTenant(task.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.changePriority(task, taskpriority);
	}

	@RequestMapping("/editdate")
	@JsonView({ TaskJsonView.class })
	public void changeDate(@RequestBody Map<String, Object> task) throws Exception {
		Task date = taskRepository.findById(task.get("id").toString());
		Project project = projectRepository.findByProjectIdAndTenant(date.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.changeDate(task);
	}

	@RequestMapping("/deletetask")
	public void deleteTask(@RequestParam String taskid) throws Exception {
		Task task = taskRepository.findByIdAndTenant(taskid, authUtilService.getLoggedInUsersTenant());
		Project project = projectRepository.findByProjectIdAndTenant(task.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.DELETE_ISSUE, project);
		taskService.deleteTask(task);
	}

	@RequestMapping("/edituser")
	//@JsonView({ UserJsonView.class, TaskJsonView.class })
	public void editUser(@RequestBody Task task) {
		taskService.updateUser(task);
	}

	@RequestMapping("/edittag")
	@JsonView({ TaskJsonView.class })
	public void updateTag(@RequestBody Task task) throws Exception {

		Project project = projectRepository.findByProjectIdAndTenant(task.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.updateTag(task);

	}

	@RequestMapping("/api/fileupload")
	@ResponseBody
	public void fileUpload(@RequestParam("file") MultipartFile file, @RequestParam("project") Long projectId,
			@RequestParam("task") String taskId) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.fileUpload(file, projectId, taskId);
	}

	@RequestMapping("/api/fileupload/comment")
	@ResponseBody
	public void fileUploadComment(@RequestParam("file") MultipartFile file, @RequestParam("project") Long projectId,
			@RequestParam("task") String taskId, @RequestParam("comment") String comment) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.fileUploadComment(file, projectId, taskId, comment);
	}

	@RequestMapping("/getfiles")
	@JsonView({ TaskJsonView.class })
	public List<String> getFiles(@RequestParam("project") Long projectId, @RequestParam("task") Long taskId) {
		List<String> files = new ArrayList<String>();
		try {
			Project project = projectRepository.findByProjectIdAndTenant(projectId,
					authUtilService.getLoggedInUsersTenant());
			projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
			files = taskService.getFiles(projectId, taskId);

		} catch (Exception e) {
			log.warn("Files not found");
		}
		return files;
	}

	@RequestMapping("/getfiles/comment")
	@JsonView({ TaskJsonView.class })
	public List<String> getCommentFiles(@RequestParam("project") Long projectId, @RequestParam("task") String taskId,
			@RequestParam("comment") String commentId) {
		List<String> files = new ArrayList<String>();
		try {
			Project project = projectRepository.findByProjectIdAndTenant(projectId,
					authUtilService.getLoggedInUsersTenant());
			projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
			files = taskService.getCommentFiles(projectId, taskId, commentId);

		} catch (Exception e) {
			log.warn("Files not found");
		}
		return files;
	}

	@RequestMapping("/fileupload/delete")
	@ResponseBody
	public HashMap<String, Object> deleteFiles(@RequestBody HashMap<String, String> filename,
			@RequestParam Long project, @RequestParam Long task) throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		return taskService.deleteFiles(project, task, filename);
	}

	@RequestMapping("/download")
	@JsonView({ TaskJsonView.class })
	public void downloadFile(@RequestParam("project") Long projectId, @RequestParam("task") Long taskId,
			@RequestParam("file") String filename) throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		taskService.downloadFile(projectId, taskId, filename);

	}

	@RequestMapping("/download/all")
	public void downloadAllFile(@RequestParam Long project, @RequestParam String task, @RequestParam String file)
			throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		taskService.downloadAllFile(project, task, file);
	}

	@RequestMapping("/subtask")
	public void subtaskStatus(@RequestParam Long project, @RequestParam Long task, @RequestParam String subtask,
			@RequestParam boolean status) throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		projectAuthUtilities.checkForAuthority(Authority.ADDING_SUBTASK, findProject);
		taskService.subtaskStatus(project, task, subtask, status);
	}

	@RequestMapping("/add/subtask")
	public void addSubtask(@RequestParam Long project, @RequestParam Long task, @RequestBody SubTask subTask)
			throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		projectAuthUtilities.checkForAuthority(Authority.ADDING_SUBTASK, findProject);
		taskService.addSubtask(project, task, subTask);
	}

	@RequestMapping("/getAll/subtask")
	public List<SubTask> getSubtask(@RequestParam String task) throws Exception {
		return taskService.getSubtask(task);
	}

	@RequestMapping("/edit/subtask")
	public void editSubtask(@RequestParam String subtask, @RequestParam Long project, @RequestParam Long task,
			@RequestBody SubTask editSubTask) throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		projectAuthUtilities.checkForAuthority(Authority.EDIT_SUBTASK, findProject);
		taskService.editSubtask(subtask, project, task, editSubTask);
	}

	@RequestMapping("/delete/subtask")
	public void deleteSubtask(@RequestParam String subtask, @RequestParam Long project, @RequestParam Long task)
			throws Exception {
		Project findProject = projectRepository.findByProjectIdAndTenant(project,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, findProject);
		projectAuthUtilities.checkForAuthority(Authority.DELETE_SUBTASK, findProject);
		taskService.deleteSubtask(subtask, project, task);
	}

	@RequestMapping("/add/Comment")
	public void addComment(@RequestParam("project") long projectId, @RequestParam("task") long taskId,
			@RequestBody String message) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.addComment(projectId, taskId, message);
	}

	@RequestMapping("/delete/Comment")
	public void deleteComment(@RequestParam("project") Long projectId, @RequestParam("task") Long taskId,
			@RequestBody String commentId) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.deleteComment(projectId, taskId, commentId);
	}

	@RequestMapping("/edit/Comment")
	public void updateComment(@RequestParam("project") Long projectId, @RequestParam("task") String taskId,
			@RequestParam("comment") String commentId, @RequestBody String message) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		projectAuthUtilities.checkForAuthority(Authority.EDIT_ISSUE, project);
		taskService.editComment(projectId, taskId, commentId, message);
	}

	@RequestMapping("/getTaskId")
	public List<String> getTaskId() {
		return taskService.getTaskId();
	}

	@JsonView({ TaskHistoryJsonView.class })
	@RequestMapping("/audit/getaudit")
	public List<String> getAudit(@RequestParam("task") Long taskId) throws Exception {
		return taskAuditService.getAudit(taskId);
	}

	@RequestMapping("/gettags")
	@JsonView(TagJsonView.class)
	public List<Tag> getTags(@RequestParam("searchletter") String searchletter,
			@RequestParam("projectId") Long projectId) {
		return taskService.getTags(searchletter, projectId);

	}

	@RequestMapping("getstatus")
	public HashMap<Status, String> getStatus() {
		return taskService.getStatus();
	}

	@RequestMapping("/getpriority")
	public HashMap<Priority, String> getPriority() {

		return taskService.getPriority();
	}

	@RequestMapping("/project/gettags")
	@JsonView(TagJsonView.class)
	public List<Tag> getTags(@RequestParam Long projectId) throws Exception {
		return taskService.getTags(projectId);
	}
}