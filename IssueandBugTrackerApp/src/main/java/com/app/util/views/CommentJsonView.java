package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;


public class CommentJsonView {

	public interface Summary {
	};

	public interface CommentReadView extends PaginatedResult {
	};
}
