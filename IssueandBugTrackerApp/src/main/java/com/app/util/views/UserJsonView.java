package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;


public class UserJsonView {

	public interface UserSumaryView {
	};

	public interface UserReadView extends PaginatedResult {
	};

}
