package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, String> {

	UserRole findByUserId(String id);

}
