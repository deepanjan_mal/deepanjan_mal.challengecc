/**
 * 
 */
package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entities.User;
import com.app.entities.VerificationToken;

@Repository
public interface TokenVerificationRepository extends JpaRepository<VerificationToken, Integer> {

	VerificationToken findByUser(User user);
}
