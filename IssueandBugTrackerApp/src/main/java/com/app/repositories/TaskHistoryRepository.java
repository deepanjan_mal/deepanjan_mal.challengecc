package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.audit.TaskHistory;

public interface TaskHistoryRepository extends JpaRepository<TaskHistory, String> {

	List<TaskHistory> findByTaskId( String taskId);
}
