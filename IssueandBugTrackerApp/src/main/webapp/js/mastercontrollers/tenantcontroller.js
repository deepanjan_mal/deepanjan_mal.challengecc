tmsApp.controller('viewtenantcontroller', function($http, $scope, $log) {
	$scope.getTenants = function() {
		$http.get('/sa/listtenant').success(function(res) {

			$scope.tenants = res;

		}).error(function(err) {
			$log.info(err);
		})
	}
	$scope.getTenants();

	$scope.loadtenants = function() {
		$http.get('/sa/gettenant?tenantname=' + $scope.searchText).success(
				function(response) {
					$scope.tenants = response;
				}).error(function(err) {
			$log.info(err);
		})
	}
})