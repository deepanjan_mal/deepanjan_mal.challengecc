tmsApp.controller('editprojectcontroller', function($scope, $http, $rootScope,
		tmsutility, toaster, ngToast, $routeParams, $timeout, $log,authorityutility) {

	tmsutility.method($scope);
	authorityutility.method($http,$routeParams.projectId).success(function(res){
		$scope.authorities=res;
	})
		
	$scope.updateProject = function() {
		var date = $('#startdate')[0].value;
		$scope.editedProject.startDate = (new Date(date)).getTime();
		$http({
			method : 'POST',
			url : '/all/project/updateproject',
			data : $scope.editedProject
		}).success(function() {

			// success code
			toaster.method("success", "Success!!", ngToast);

			$timeout(function() {
				location.href = "#/project";

			}, 2000);

		}).error(function(err) {
			$log.info(err);
			// error code
			toaster.method("danger", "Error!!", ngToast);

			$timeout(function() {
				location.href = "#/project";

			}, 500);

		})
	}

	$scope.getProject = function() {
		$http
				.get(
						'/all/project/getproject?projectId='
								+ $routeParams.projectId).success(
						function(response) {

							$scope.editedProject = response
						}).error(function(err) {
					$log.info(err);
				})

	}
	$scope.getProject();
})