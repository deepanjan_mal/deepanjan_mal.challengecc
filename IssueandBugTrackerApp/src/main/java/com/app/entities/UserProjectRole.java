package com.app.entities;
/**
 * @author Deepanjan
 */
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.app.util.views.LoadUserJsonVew;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "user_project_roles", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"user_id", "project_id" }) })
public class UserProjectRole extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JsonView({ UserProjectRoleJsonView.class, LoadUserJsonVew.class})
	User user;

	@ManyToOne
	@JsonView({ ProjectJsonView.class })
	Project project;

	@ManyToOne
	@JsonView({ UserProjectRoleJsonView.class })
	ProjectRole projectRole;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ProjectRole getProjectRole() {
		return projectRole;
	}

	public void setProjectRole(ProjectRole projectRole) {
		this.projectRole = projectRole;
	}

}
