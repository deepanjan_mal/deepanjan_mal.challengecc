/**
 * 
 */
package com.app.enums;

/**
 * @author Deepanjan
 */
public enum TenantRole {
	
	PROJECT_LEAD("Project Lead"),
	DEVELOPER("Developer"),
	REPORTER("Reporter"),
	PROJECT_MANAGER("Project Manager");
	
	String value;

	/**
	 * @param value
	 */
	private TenantRole(String value) {
		this.value = value;
	}
	
	public String toString(){
		return this.value;
	}
	
	

}
