package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.Tenant;
import com.app.entities.User;

/**
 * It extends JpaRepository to perform CRUD operations
 *
 */
public interface UserRepository extends JpaRepository<User, String> {

	User findByUsernameAndTenantDomain(String username, String domain);

	User findById(String userid);

	List<User> findByUsernameIgnoreCaseContainingAndTenant(String name,
			Tenant tenant);

	List<User> findByTenant(Tenant loggedInUsersTenant);

	User findByUsernameAndTenant(String username, Tenant loggedInUsersTenant);

	User findByEmail(String email);

	User findByEmailAndTenant(String email, Tenant tenantObj);

}
