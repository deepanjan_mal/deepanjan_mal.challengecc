package com.app.entities;
/**
 * @author Deepanjan
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "subtasks")
public class SubTask extends AbstractEntity {

	private static final long serialVersionUID = -4915042411090060810L;

	@Column(name = "message")
	@JsonView({ TaskJsonView.class })
	private String message;

	@Column(name = "isComplete")
	@JsonView({ TaskJsonView.class })
	private boolean isComplete= false;

	@ManyToOne
	@JoinColumn(name="task_id")
	Task task;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public boolean isComplete() {
		return isComplete;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

}
