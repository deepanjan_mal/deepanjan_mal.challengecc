/**
 * 
 */
package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.SubTask;

public interface SubTaskRepository extends JpaRepository<SubTask, String> {
	SubTask findByIdAndTaskId(String subTaskId, String taskId);
	List<SubTask> findByTaskId(String taskId);
	/**
	 * @param subtask
	 * @return
	 */
	SubTask findById(String subtask);

}
