package com.app.task.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.entities.Comment;
import com.app.entities.Project;
import com.app.entities.SubTask;
import com.app.entities.Tag;
import com.app.entities.Task;
import com.app.entities.TaskHistoryContent;
import com.app.entities.User;
import com.app.entities.audit.TaskHistory;
import com.app.enums.Priority;
import com.app.enums.Status;
import com.app.enums.TaskAudit;
import com.app.repositories.CommentRepository;
import com.app.repositories.ProjectRepository;
import com.app.repositories.SubTaskRepository;
import com.app.repositories.TagRepository;
import com.app.repositories.TaskPropertyContentRepository;
import com.app.repositories.TaskPropertyRepository;
import com.app.repositories.TaskRepository;
import com.app.repositories.UserProjectRolesRepository;
import com.app.repositories.UserRepository;
import com.app.tokenverifcation.services.TokenVerificationServices;
import com.app.util.AuditingUtilities;
import com.app.util.AuthUtilService;
import com.app.util.DefaultRestException;
import com.app.util.Utilities;

/**
 * @author deepanjan
 */
@Service
public class TaskService {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	TagRepository tagRepository;

	@Autowired
	SubTaskRepository subTaskRepository;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	TaskPropertyRepository taskPropertyRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	TaskPropertyContentRepository taskPropertyContentRepository;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	@Autowired
	TaskHistoryService taskHistoryService;

	@Autowired
	TokenVerificationServices tokenVerificationServices;

	@Value("${multipart.location}")
	private String location;

	@Value("${spring.application-path}")
	private String taskLink;

	@Autowired
	public void createFileUploadDirectory() {
		if (!new File(location).exists()) {
			new File(location).mkdirs();
			log.info("Directory Created");
		} else if (new File(location).exists()) {
			log.info("Directory exists");
		} else {
			log.info("Directory creation failed");
		}
	}

	public Map<String, Object> addTask(Task task, Project project) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		task.setProject(project);
		task.setTenant(authUtilService.getLoggedInUsersTenant());
		task.setStatus(Status.ACTIVE);
		task.setPriority(Priority.LOW);
		taskRepository.save(task);

		taskHistoryService.addAuditUtility(TaskAudit.TITLE, task, task.getTitle());
		taskHistoryService.addAuditUtility(TaskAudit.STATUS, task, task.getStatus().toString());
		taskHistoryService.addAuditUtility(TaskAudit.DESCRIPTION, task, task.getDescription());
		taskHistoryService.addAuditUtility(TaskAudit.PRIORITY, task, task.getPriority().toString());
		return map;
	}

	/**
	 * @param value
	 * @return List of tasks based on value
	 */
	public List<Task> searchTasks(String value) {
		return taskRepository.findByTitleIgnoreCaseContainingAndIsDelete(value, false);
	}

	public List<Task> getTasks(List<Status> status, List<Priority> priority, long projectId, String deadline,
			List<String> tags) {
		List<Task> task = new ArrayList<>();
		if (projectId == 0) {
			task = taskRepository.findByTenant(authUtilService.getLoggedInUsersTenant());
		} else {
			Project project = projectRepository.findByProjectIdAndTenant(projectId,
					authUtilService.getLoggedInUsersTenant());
			task = taskRepository.findByProject(project);
		}
		return task;
	}

	public List<Task> getMyTasks(List<Status> status, List<Priority> priority, long projectId, String deadline,
			String userid) {

		return taskRepository.findByUsers(authUtilService.getLoggedInUser());
	}

	public Task getTask(long id) {

		return taskRepository.findByTaskIdAndTenant(id, authUtilService.getLoggedInUsersTenant());

	}

	public void updateTask(Map<String, Object> taskDetails)
			throws ParseException, IllegalArgumentException, IllegalAccessException {
		String oldTitle = null, oldDescription = null;
		Task task = taskRepository.findById(taskDetails.get("id").toString());
		if (taskDetails.containsKey("title")) {
			oldTitle = task.getTitle();
			task.setTitle(taskDetails.get("title").toString());

		} else if (taskDetails.containsKey("description")) {
			oldDescription = task.getDescription();
			task.setDescription(taskDetails.get("description").toString());
		}

		taskRepository.save(task);
		if (taskDetails.containsKey("title")) {
			taskHistoryService.addAuditUtility(TaskAudit.TITLE, task, task.getTitle(), oldTitle);
		}
		if (taskDetails.containsKey("description")) {
			taskHistoryService.addAuditUtility(TaskAudit.DESCRIPTION, task, task.getDescription(), oldDescription);
		}

	}

	public void changeStatus(Task task, Task taskstatus) {

		try {
			SubTask subTask = new SubTask();
			String oldStatus = task.getStatus().toString();
			List<SubTask> subtasks = subTaskRepository.findByTaskId(taskstatus.getId());
			if (taskstatus.getStatus().toString().equalsIgnoreCase(Status.CLOSE.name())) {
				for (int i = 0; i < subtasks.size(); i++) {
					subTask = subtasks.get(i);
					subTask.setComplete(true);
				}
				task.setStatus(taskstatus.getStatus());
				task.setCompleteDate(Utilities.getCurrentDateAndTime());
				taskRepository.save(task);
				taskHistoryService.addAuditUtility(TaskAudit.COMPLETEDATE, task, task.getCompleteDate().toString());

			} else {
				task.setStatus(taskstatus.getStatus());
				taskRepository.save(task);

			}
			taskHistoryService.addAuditUtility(TaskAudit.STATUS, task, task.getStatus().toString(), oldStatus);

		} catch (Exception e) {
		}
	}

	public void changePriority(Task task, Task taskpriority) {
		try {
			String oldPriority = null;
			if (task.getPriority() != null)
				oldPriority = task.getPriority().toString();
			task.setPriority(taskpriority.getPriority());
			taskRepository.save(task);
			taskHistoryService.addAuditUtility(TaskAudit.PRIORITY, task, task.getPriority().toString(), oldPriority);
		} catch (Exception e) {
		}
	}

	public void changeDate(Map<String, Object> task) {
		try {
			Date oldStartDate = null, oldDeadline = null;
			Task changedtask = taskRepository.findById(task.get("id").toString());
			if (task.containsKey("startDate")) {

				oldStartDate = changedtask.getStartDate();
				changedtask.setStartDate(new Date((long) task.get("startDate")));
			} else if (task.containsKey("deadLine")) {
				oldDeadline = changedtask.getDeadline();
				changedtask.setDeadline(new Date((long) task.get("deadLine")));
			}

			taskRepository.save(changedtask);
			if (task.containsKey("startDate")) {
				if (oldStartDate != null)
					taskHistoryService.addAuditUtility(TaskAudit.STARTDATE, changedtask,
							changedtask.getStartDate().toString(), oldStartDate.toString());
				else
					taskHistoryService.addAuditUtility(TaskAudit.STARTDATE, changedtask,
							changedtask.getStartDate().toString());
			} else if (task.containsKey("deadLine")) {
				if (oldDeadline != null)
					taskHistoryService.addAuditUtility(TaskAudit.DEADLINE, changedtask,
							changedtask.getDeadline().toString(), oldDeadline.toString());
				else
					taskHistoryService.addAuditUtility(TaskAudit.DEADLINE, changedtask,
							changedtask.getDeadline().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteTask(Task task) throws IOException, ParseException {

		List<TaskHistory> taskProperty = taskPropertyRepository.findByTaskId(task.getId());
		TaskHistoryContent taskPropertyContent = new TaskHistoryContent();
		taskPropertyContent.setId(UUID.randomUUID().toString());
		taskPropertyContent.setTaskProperty(AuditingUtilities.archieveTaskAuditContents(taskProperty));
		taskPropertyContent.setTask(task);
		taskPropertyContentRepository.save(taskPropertyContent);
		taskPropertyRepository.delete(taskProperty);
		task.setIsDelete(true);
		taskRepository.save(task);
	}

	public void updateTag(Task taskobj) {

		Task task = taskRepository.findByTaskIdAndTenant(taskobj.getTaskId(), authUtilService.getLoggedInUsersTenant());
		List<Tag> taglist = new ArrayList<Tag>();
		Project projectObj = projectRepository.findByProjectIdAndTenant(taskobj.getProject().getProjectId(),
				authUtilService.getLoggedInUsersTenant());
		for (Tag tag : taskobj.getTags()) {
			Tag tagObj = tagRepository.findByTagNameIgnoreCaseContainingAndProject(tag.getTagName(), projectObj);
			if (tagObj != null) {
				taglist.add(tagObj);
				task.setTags(taglist);
				taskRepository.save(task);
			} else {

				tag.setTagName(tag.getTagName());
				tag.setProject(projectObj);
				tagRepository.save(tag);

				taglist.add(tag);
				task.setTags(taglist);
				taskRepository.save(task);
			}
		}
	}

	/**
	 * 
	 * @param file
	 * @param projectId
	 * @param taskId
	 * @comments
	 */
	public void fileUpload(MultipartFile file, Long projectId, String taskId) {
		if (!file.isEmpty()) {
			try {
				String uploadsDir = location + "/uploads/" + "tenant/" + authUtilService.getLoggedInUsersTenantName()
						+ "/project/" + projectId + "/task/" + taskId + "/";

				if (!new File(uploadsDir).exists()) {
					new File(uploadsDir).mkdirs();
					log.info("Directory Created");
				}
				log.info("realPathtoUploads = {}", uploadsDir);
				String originalFileName = file.getOriginalFilename();
				String filePath = uploadsDir + originalFileName;
				File destination = new File(filePath);
				file.transferTo(destination);

				Task task = taskRepository.findByIdAndTenant(taskId, authUtilService.getLoggedInUsersTenant());
				taskHistoryService.addAuditUtility(TaskAudit.FILE, task, originalFileName);

			} catch (Exception e) {
			}

		} else {
			log.info("File upload Failed!! Try again.....");
		}
	}

	/**
	 * @param file
	 * @param projectId
	 * @param taskId
	 * @comments
	 */
	public void fileUploadComment(MultipartFile file, Long projectId, String taskId, String commentId) {

		if (!file.isEmpty()) {
			try {
				String uploadsDir = location + "/uploads/" + "tenant/" + authUtilService.getLoggedInUsersTenantName()
						+ "/project/" + projectId + "/task/" + taskId + "/comment/" + commentId + "/";

				if (!new File(uploadsDir).exists()) {
					new File(uploadsDir).mkdirs();
					log.info("Directory Created");
				}
				log.info("realPathtoUploads = {}", uploadsDir);
				String originalFileName = file.getOriginalFilename();
				String filePath = uploadsDir + originalFileName;
				File destination = new File(filePath);
				file.transferTo(destination);

				taskHistoryService.addAuditUtility(TaskAudit.FILE,
						taskRepository.findByIdAndTenant(taskId, authUtilService.getLoggedInUsersTenant()),
						originalFileName);

			} catch (Exception e) {
			}

		} else {
			log.info("File upload Failed!! Try again.....");
		}
	}

	/*
	 * List of files for task
	 */
	public List<String> getFiles(Long projectId, Long taskId) {
		String tenant = authUtilService.getLoggedInUsersTenantName();
		List<String> list = new ArrayList<String>();

		String filepath = location + "/uploads/" + "tenant/" + tenant + "/project/" + projectId + "/task/" + taskId;

		File directory = new File(filepath);
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				list.add(file.getName());
			}
		}
		return list;
	}

	/*
	 * List of files attached to comments
	 */
	public List<String> getCommentFiles(Long projectId, String taskId, String commentId) {
		String tenant = authUtilService.getLoggedInUsersTenantName();
		List<String> list = new ArrayList<String>();

		String filepath = location + "/uploads/" + "tenant/" + tenant + "/project/" + projectId + "/task/" + taskId
				+ "/comment/" + commentId;

		File directory = new File(filepath);
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				list.add(file.getName());
			}
		}

		return list;
	}

	/**
	 * @param project
	 * @param task
	 * @param filename
	 * @return
	 * @throws ParseException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public HashMap<String, Object> deleteFiles(Long project, Long task, HashMap<String, String> filename)
			throws ParseException, IllegalArgumentException, IllegalAccessException {
		HashMap<String, Object> map = new HashMap<String, Object>();

		String filepath = location + "/uploads/" + "tenant/" + authUtilService.getLoggedInUsersTenantName()
				+ "/project/" + project + "/task/" + task + "/";

		File file = new File(filepath + filename.get("filename"));
		if (file.delete()) {
			log.info("File " + filename.get("filename") + " has been deleted successfully");
			map.put("message", "File has been deleted succesfully");

			taskHistoryService.addAuditUtility(TaskAudit.FILE,
					taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant()), null,
					filename.get("filename"));

		} else {
			map.put("message", "Couldn't delete file");
		}
		return map;
	}

	// download single file
	/**
	 * @param project
	 * @param task
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public void downloadFile(Long projectId, Long taskId, String filename) throws Exception {
		String tenant = authUtilService.getLoggedInUsersTenantName();

		String filePath = location + "/uploads/" + "tenant/" + tenant + "/project/" + projectId + "/task/" + taskId
				+ "/" + filename;

		// get absolute path of the application
		ServletContext context = request.getServletContext();
		// String appPath = context.getRealPath("");

		// construct the complete absolute path of the file
		String fullPath = filePath;
		File downloadFile = new File(fullPath);
		FileInputStream inputStream = new FileInputStream(downloadFile);

		// get MIME type of the file
		String mimeType = context.getMimeType(filename);
		if (mimeType == null) {
			mimeType = "application/octet-stream";
		}

		// set content attributes for the response
		response.setContentType(mimeType);
		response.setContentLength((int) downloadFile.length());

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
		response.setHeader(headerKey, headerValue);

		// get output stream of the response
		OutputStream outStream = response.getOutputStream();
		IOUtils.copy(inputStream, outStream);

		inputStream.close();
		outStream.close();

	}

	// Download all files in directory

	/**
	 * @param project
	 * @param task
	 * @return
	 */
	public void downloadAllFile(Long project, String task, String file) throws Exception {

		TaskService multipleFileZip = new TaskService();
		List<String> files = new ArrayList<String>();

		String filepath = location + "/uploads/tenant/" + authUtilService.getLoggedInUsersTenantName() + "/project/"
				+ project + "/task/" + task;

		File dir = new File(filepath);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				files.add(child.toString());
			}
		} else {
			log.info("Directory not available");
		}

		multipleFileZip.zipFiles(files, project, task, file, authUtilService.getLoggedInUsersTenantName(), location);
		// multipleFileZip.downloadfile(project, task, allFileDownload);
	}

	/**
	 * iterates over the files present in the directory and request for zipping
	 * 
	 * @param files
	 * @param project
	 * @param task
	 * @param file
	 * @param tenant
	 */
	public void zipFiles(List<String> files, Long project, String task, String file, String tenant, String location) {

		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		try {
			String filepath = location + "/uploads/tenant/" + tenant + "/project/" + project + "/task/" + task + "/"
					+ file;

			fos = new FileOutputStream(filepath);
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (String filePath : files) {
				File input = new File(filePath);
				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[8 * 1024];
				int size = 0;
				while ((size = fis.read(tmp)) != -1) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			log.info("Zipped the files...");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null)
					fos.close();
			} catch (Exception ex) {

			}
		}
	}

	/**
	 * @param project
	 * @param task
	 * @param subTask
	 * @throws Exception
	 */
	public void addSubtask(Long project, Long task, SubTask subTask) throws Exception {
		if (!subTask.getMessage().isEmpty()) {
			boolean flag = true;
			Task taskObj = taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant());
			String taskId = taskObj.getId();
			List<SubTask> subTaskList = subTaskRepository.findByTaskId(taskId);
			if (!subTaskList.isEmpty()) {
				for (int i = 0; i < subTaskList.size(); i++) {
					if (subTaskList.get(i).getMessage().equalsIgnoreCase(subTask.getMessage())) {
						flag = true;
						break;
					} else {
						flag = false;
					}
				}
			}
			if (subTaskList.isEmpty() || flag == false) {
				taskObj.setStatus(Status.ACTIVE);
				subTask.setId(UUID.randomUUID().toString());
				subTask.setTask(taskObj);
				subTaskRepository.save(subTask);
				taskHistoryService.addAuditUtility(TaskAudit.SUBTASK, taskObj, subTask.getMessage());

			}
		}
	}

	/**
	 * @param subtask
	 *            Id
	 * @param project
	 * @param task
	 * @param editSubTask
	 */
	public void editSubtask(String subtask, Long project, Long task, SubTask editSubTask) throws Exception {
		String taskId = taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant()).getId();
		SubTask subTask = subTaskRepository.findByIdAndTaskId(subtask, taskId);
		String oldValue = subTask.getMessage();
		subTask.setMessage(editSubTask.getMessage());
		subTaskRepository.save(subTask);
		taskHistoryService.addAuditUtility(TaskAudit.SUBTASK, subTask.getTask(), editSubTask.getMessage(), oldValue);
	}

	/**
	 * @param subtask
	 * @param project
	 * @param task
	 * @param subTask2
	 */
	public void deleteSubtask(String subtask, Long project, Long task) throws Exception {
		String taskId = taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant()).getId();
		SubTask subTask = subTaskRepository.findByIdAndTaskId(subtask, taskId);
		TaskHistory taskProperty = taskPropertyRepository.findByNewValueAndTaskId(subTask.getMessage(), taskId);
		subTaskRepository.delete(subTask);
		taskHistoryService.addAuditUtility(TaskAudit.SUBTASK, subTask.getTask(), null, taskProperty.getNewValue());
	}

	/**
	 * @param task
	 * @return
	 */
	public List<SubTask> getSubtask(String task) {
		List<SubTask> subtask = subTaskRepository.findByTaskId(task);
		return subtask;
	}

	/**
	 * @param project
	 * @param task
	 * @param subtask
	 * @param status
	 * @throws ParseException
	 */
	public void subtaskStatus(Long project, Long task, String subtask, boolean status) { // subtaskStatus
																							// history
																							// left
		try {
			boolean flag = true;
			SubTask subTask = subTaskRepository.findById(subtask);
			subTask.setComplete(status);
			subTaskRepository.save(subTask);
			Task taskId = taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant());
			List<SubTask> subtasks = subTaskRepository.findByTaskId(taskId.getId());
			for (int i = 0; i < subtasks.size(); i++) {

				if (subtasks.get(i).isComplete() == true) {
					flag = true;

				} else {
					flag = false;
					break;
				}
			}
			Task taskStatus = taskRepository.findByTaskIdAndTenant(task, authUtilService.getLoggedInUsersTenant());
			if (flag == true) {
				String oldStatus = taskStatus.getStatus().toString();
				taskStatus.setStatus(Status.COMPLETED);
				taskRepository.save(taskStatus);

				taskHistoryService.addAuditUtility(TaskAudit.STATUS, taskStatus, "Completed", oldStatus);

			} else {
				String oldStatus = taskStatus.getStatus().toString();
				taskStatus.setStatus(Status.ACTIVE);
				taskRepository.save(taskStatus);
				taskHistoryService.addAuditUtility(TaskAudit.STATUS, taskStatus, "Active", oldStatus);
			}

		} catch (Exception e) {

		}
	}

	// Comment
	public void addComment(Long projectId, Long taskId, String title)
			throws ParseException, IllegalArgumentException, IllegalAccessException {

		Comment comment = new Comment();
		Task task = taskRepository.findByTaskIdAndTenant(taskId, authUtilService.getLoggedInUsersTenant());
		comment.setId(UUID.randomUUID().toString());
		comment.setMessage(title);
		comment.setTask(task);
		commentRepository.save(comment);

		taskHistoryService.addAuditUtility(TaskAudit.COMMENT, task, title);
	}

	public void deleteComment(Long projectId, Long taskId, String commentId)
			throws ParseException, IllegalArgumentException, IllegalAccessException {
		Comment comment = new Comment();
		Task task = new Task();
		task = taskRepository.findByTaskIdAndTenant(taskId, authUtilService.getLoggedInUsersTenant());
		comment = commentRepository.findById(commentId);
		String oldValue = comment.getMessage();
		commentRepository.delete(comment);

		taskHistoryService.addAuditUtility(TaskAudit.COMMENT, task, null, oldValue);
	}

	public void editComment(Long projectId, String taskId, String commentId, String message)
			throws ParseException, IllegalArgumentException, IllegalAccessException {
		Comment comment = new Comment();
		Task task = new Task();
		task = taskRepository.findByIdAndTenant(taskId, authUtilService.getLoggedInUsersTenant());
		comment = commentRepository.findById(commentId);
		String oldValue = comment.getMessage();
		comment.setMessage(message);
		commentRepository.save(comment);

		taskHistoryService.addAuditUtility(TaskAudit.COMMENT, task, message, oldValue);
	}

	public List<String> getTaskId() {
		List<Task> tasks = new ArrayList<Task>();
		tasks = taskRepository.findByTenant(authUtilService.getLoggedInUsersTenant());
		Iterator<Task> iterator = tasks.iterator();
		List<String> taskId = new ArrayList<String>();
		while (iterator.hasNext()) {
			taskId.add(Long.toString(iterator.next().getTaskId()));
		}
		return taskId;
	}

	/**
	 * @param task
	 * 
	 */
	public void updateUser(Task task) {
		try {
			Task taskAdd = taskRepository.findByTaskIdAndTenant(Long.parseLong(task.getId()),
					authUtilService.getLoggedInUsersTenant());
			String link = taskLink + "/project/"+taskAdd.getProject().getProjectId()+"/task/"+taskAdd.getTaskId();
			List<User> userList = new ArrayList<User>();
			for (int i = 0; i < (task.getUsers().size()); i++) {
				String email = (userRepository.findById(task.getUsers().get(i).getId())).getEmail();
				userList.add(task.getUsers().get(i));
				taskAdd.setUsers(userList);
				taskRepository.save(taskAdd);
				taskHistoryService.addAuditUtility(TaskAudit.USER, taskAdd, task.getUsers().get(i).getName());

				new Thread() {
					public void run() {
						try {
							tokenVerificationServices.sendEmail(email, link);
						} catch (Exception v) {
							v.printStackTrace();
						}
					}
				}.start();
			}

		} catch (Exception ex) {
			log.info(ex.getMessage());
		}
	}

	public List<Tag> getTags(String searchletter, Long projectId) {
		return tagRepository.findByTagNameIgnoreCaseContainingAndProjectProjectId(searchletter, projectId);

	}

	public HashMap<Status, String> getStatus() {
		HashMap<Status, String> object = new HashMap<Status, String>();
		for (Status status : Status.values()) {
			object.put(status, status.value());
		}
		return object;
	}

	public HashMap<Priority, String> getPriority() {
		HashMap<Priority, String> object = new HashMap<Priority, String>();
		for (Priority priority : Priority.values()) {
			object.put(priority, priority.value());
		}
		return object;
	}

	/**
	 * @param projectId
	 * @return
	 */
	public List<Tag> getTags(Long projectId) throws Exception {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		List<Tag> tags = tagRepository.findByProject(project);
		return tags;
	}

	@ExceptionHandler(DefaultRestException.class)
	@ResponseBody
	public Map<String, Object> errorResponse(DefaultRestException ex) {
		return ex.getMap();
	}

}
