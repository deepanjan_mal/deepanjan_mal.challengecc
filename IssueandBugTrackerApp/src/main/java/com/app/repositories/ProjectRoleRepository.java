package com.app.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.ProjectRole;
import com.app.entities.Tenant;

public interface ProjectRoleRepository extends
		JpaRepository<ProjectRole, String> {
	List<ProjectRole> findByTenant(Tenant tenant);

	ProjectRole findByIdAndTenant(String roleId, Tenant loggedInUsersTenant);

	ProjectRole findById(String id);

}