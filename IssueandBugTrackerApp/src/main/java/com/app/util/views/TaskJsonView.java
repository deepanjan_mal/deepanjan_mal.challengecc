package com.app.util.views;

import com.app.util.CommonJsonView.PaginatedResult;


public class TaskJsonView {

	public interface Summary {
	};

	public interface TaskReadView extends PaginatedResult {
	};
}
