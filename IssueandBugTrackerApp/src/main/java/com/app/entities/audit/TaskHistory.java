package com.app.entities.audit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.app.entities.AbstractEntity;
import com.app.entities.Task;
import com.app.enums.TaskAudit;
import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author Deepanjan
 */
@Entity
@Table(name = "task_audit")
public class TaskHistory extends AbstractEntity {

	private static final long serialVersionUID = -2397647633044065220L;

	@Column(name = "new_value")
	@JsonView({ TaskJsonView.class })
	String newValue;

	@Column(name = "old_value")
	@JsonView({ TaskJsonView.class })
	String oldValue;

	@Column(name = "task_property")
	@Enumerated(EnumType.STRING)
	@JsonView({ TaskJsonView.class })
	private TaskAudit auditTaskProperty;

	@ManyToOne
	@JoinColumn(name = "task_id")
	private Task task;

	public TaskAudit getAuditTaskProperty() {
		return auditTaskProperty;
	}

	public void setAuditTaskProperty(TaskAudit auditTaskProperty) {
		this.auditTaskProperty = auditTaskProperty;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

}
