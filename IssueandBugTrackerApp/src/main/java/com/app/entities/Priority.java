package com.app.entities;
/**
 * @author Deepanjan
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.app.util.views.PriorityJsonView;
import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "priorities")
public class Priority extends AbstractTenantEntity {

	private static final long serialVersionUID = -4495416398887677457L;

	@Column(name = "priority_type")
	@JsonView({TaskJsonView.class, PriorityJsonView.class})
	private String priorityType;



	public String getPriorityType() {
		return priorityType;
	}

	public void setPriorityType(String priorityType) {
		this.priorityType = priorityType;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
