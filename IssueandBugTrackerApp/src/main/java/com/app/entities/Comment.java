package com.app.entities;
/**
 * @author Deepanjan
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "comments")
public class Comment extends AbstractEntity {

	private static final long serialVersionUID = 9140842041173740417L;

	@Column(name = "message")
	@JsonView({TaskJsonView.class})
	private String message;

	
	@ManyToOne
	@JoinColumn(name="task_id")
	Task task;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
