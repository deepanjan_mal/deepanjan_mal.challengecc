package com.app.master.services;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.auth.services.AuthService;
import com.app.entities.Tenant;
import com.app.entities.User;
import com.app.entities.UserRole;
import com.app.entities.VerificationToken;
import com.app.enums.UserEnable;
import com.app.project.services.ProjectRoleService;
import com.app.repositories.TenantRepository;
import com.app.repositories.TokenVerificationRepository;
import com.app.repositories.UserRepository;
import com.app.repositories.UserRoleRepository;
import com.app.tokenverifcation.services.TokenVerificationServices;
import com.app.util.TokenVerificationUtiities;

/**
 * @author deepanjan
 *
 */

@Service
public class MasterService {

	public Logger logger = LoggerFactory.getLogger(MasterService.class);

	@Autowired
	TenantRepository tenantRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Value("${spring.application-path}")
	private String hostName;

	@Autowired
	TokenVerificationServices tokenVerificationServices;

	@Autowired
	TokenVerificationRepository tokenVerificationRepository;

	@Autowired
	ProjectRoleService projectRoleService;

	@Autowired
	AuthService authService;

	/**
	 * Adding tenant by a super user
	 * 
	 * @param tenant
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> addTenant(Tenant tenant, User user)
			throws Exception {

		HashMap<String, Object> mapValues = new HashMap<String, Object>();

		String token = TokenVerificationUtiities.generateToken();
		String url = TokenVerificationUtiities.generateTenantLink(hostName,
				token, tenant.getTenantName());
		String link = TokenVerificationUtiities.generateLink(hostName, token,
				user.getId());
		tenant.setTenantUrl(url);
		tenantRepository.save(tenant);
		projectRoleService.addDefaultTenantRole(tenant);
		user.setTenant(tenant);
		user.setEnabled(true);
		user.setStatus(UserEnable.PENDING);
		userRepository.save(user);

		UserRole userRole = new UserRole();
		userRole.setRoleType("admin");
		userRole.setUser(user);
		userRoleRepository.save(userRole);
		VerificationToken verificationToken = authService
				.setVerificationTokenAndSendMail(new VerificationToken(), user);
		mapValues.put("success", "Tenant added successfully");
		sendMail(user.getEmail(), user.getUsername(), user.getTenant()
				.getTenantUrl(), link, verificationToken.getToken());
		return mapValues;

	}

	/**
	 * 
	 * sends tenant mail for activation of admin
	 * 
	 * @param email
	 * @param username
	 * @param tenantUrl
	 * @param link
	 * @param token
	 * @throws Exception
	 */
	@Async
	public void sendMail(String email, String username, String tenantUrl,
			String link, String token) throws Exception {
		new Thread() {
			public void run() {
				try {
					tokenVerificationServices.sendTenantEmail(email, username,
							tenantUrl,  token);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Search Tenant by tenantname
	 * 
	 * @param tenantname
	 * @return List<Tenant>
	 * 
	 */
	public List<Tenant> getTenant(String tenantname) {
		return tenantRepository
				.findByTenantNameIgnoreCaseContaining(tenantname);
	}

	/**
	 * Get List of all tenants
	 * 
	 * @return List<Tenant>
	 */
	public List<Tenant> getListOfTenants() {
		return tenantRepository.findAll();
	}

}
