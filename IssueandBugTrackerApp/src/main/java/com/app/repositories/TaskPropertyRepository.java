/**
 * 
 */
package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.audit.TaskHistory;
import com.app.enums.TaskAudit;


public interface TaskPropertyRepository extends JpaRepository<TaskHistory, String> {

	List<TaskHistory> findByAuditTaskPropertyAndTaskId(TaskAudit users, String id);

	/**
	 * @param id
	 * @return
	 */
	List<TaskHistory> findByTaskId(String id);
	
	TaskHistory findByNewValueAndTaskId(String value, String taskId);

}
