package com.app.entities;
/**
 * @author Deepanjan
 */
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.app.util.views.ProjectJsonView;
import com.app.util.views.TagJsonView;
import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "tags", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"tag_name", "project_id" }) })
public class Tag extends AbstractEntity {

	private static final long serialVersionUID = -3074174504713983306L;

	@Column(name = "tag_name")
	@JsonView({ ProjectJsonView.class, TaskJsonView.class,TagJsonView.class })
	public String tagName;

	@ManyToMany
	@JoinTable(name = "tasks_tags", joinColumns = @JoinColumn(name = "tagid"), inverseJoinColumns = @JoinColumn(name = "taskid"))
	List<Task> task;

	@ManyToOne
	@JoinColumn(name="project_id")
	Project project;

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}


	public List<Task> getTask() {
		return task;
	}

	
	public void setTask(List<Task> task) {
		this.task = task;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
