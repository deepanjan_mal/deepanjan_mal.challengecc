package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.Project;
import com.app.entities.Tag;

public interface TagRepository extends JpaRepository<Tag, String> {

	List<Tag> findByTaskId(String id);

	/**
	 * @param searchletter
	 * @return
	 */
	List<Tag> findByTagNameIgnoreCaseContaining(String searchletter);

	List<Tag> findByTagNameIgnoreCaseContainingAndProjectProjectId(
			String searchletter, Long projectId);
	Tag findByTagNameIgnoreCaseContainingAndProject(String tagName, Project porject);
	/**
	 * @param project
	 * @return
	 */
	List<Tag> findByProject(Project project);

}
