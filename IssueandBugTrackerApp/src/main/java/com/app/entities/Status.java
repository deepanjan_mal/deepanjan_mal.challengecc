package com.app.entities;
/**
 * @author Deepanjan
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.app.util.views.StatusJsonView;
import com.app.util.views.TaskJsonView;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "status")
public class Status extends AbstractTenantEntity {

	private static final long serialVersionUID = 5862067206157599646L;

	@Column(name = "status_name")
	@JsonView({ TaskJsonView.class, StatusJsonView.class })
	private String statusName;

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
