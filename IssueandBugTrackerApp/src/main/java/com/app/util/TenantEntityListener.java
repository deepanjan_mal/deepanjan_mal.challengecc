package com.app.util;
/**
 * @author Deepanjan
 */
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.app.entities.AbstractTenantEntity;
import com.app.repositories.TaskRepository;

public class TenantEntityListener {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	TaskRepository taskRepository;

	/**
	 * Method will be called before saving to database. {@link AuthUtilService}
	 * object will be autowired manually into code in the body of the method as
	 * entity listeners are instantiated by JPA before Spring's autowiring
	 * happens.
	 * 
	 * @param entityObj
	 */

	@PrePersist
	@PreUpdate
	public void setTenant(AbstractTenantEntity entityObj) {
		logger.info("Setting tenant for entity");
		InjectionHelper.autowire(this, this.authUtilService);

		/*
		 * int maxId=taskRepository.findByTenantMaxTaskId(authUtilService.
		 * getLoggedInUsersTenant()); System.out.println(
		 * "=========================================================="+maxId);
		 * Task task=new Task(); task.setTaskId(maxId+1);
		 */

		logger.info("Only set the tenant if the user is not a super user or if tenant value is null");
		if (entityObj.tenant == null) {
			entityObj.setTenant(authUtilService.getLoggedInUsersTenant());
		}
	}
}
