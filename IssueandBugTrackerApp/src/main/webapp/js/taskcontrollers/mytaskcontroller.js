tmsApp.controller('mytaskcontroller',
		function($scope, $rootScope, $http, $location, tmsutility,
				$routeParams, createtask, toaster, ngToast, $log,
				authorityutility) {
	
			if ($routeParams.projectId) {
				authorityutility.method($http, $routeParams.projectId).success(
						function(res) {
							$scope.authorities = res;
						})
			} else {
				$rootScope.authorities = {
					CREATE_TASK : true
				}
			}

			$('#drop-down').click(function(event) {
				event.stopPropagation();
			});

			$scope.late = "";
			$scope.status = [];
			$scope.priority = [];
			$scope.filterTags = [];

			$scope.createFilter = function(obj) {

				if (obj.type == "priority"
						&& $scope.priority.indexOf(obj.value) == -1) {
					$scope.priority.push(obj.value)
					$scope.getTasks()
				}
				if (obj.type == "status"
						&& $scope.status.indexOf(obj.value) == -1) {

					$scope.status.push(obj.value)
					$scope.getTasks()
				}
				if (obj == "deadlineReached") {

					$scope.late = 'true';
					$scope.getTasks()
				}

				if (obj == "tag") {
					$scope.filterTags.push({
						'id' : $scope.customSelected.id,
						'tagName' : $scope.customSelected.tagName
					})
					$scope.getTasks();
				}
			}

			$scope.removeFilter = function(obj, type) {
				if (type == "status") {

					var index = $scope.status.indexOf(obj)
					$scope.status.splice(index, 1);
					$scope.getTasks();
				}
				if (type == "priority") {
					var index = $scope.priority.indexOf(obj)
					$scope.priority.splice(index, 1);
					$scope.getTasks();
				}
				if (type == "deadlineReached") {
					$scope.late = "";
					$scope.getTasks();
				}

				if (type == "tag") {
					var index = $scope.filterTags.indexOf(obj);
					$scope.filterTags.splice(index, 1);
					$scope.getTasks();
				}

			}

			if ($routeParams.projectId) {
				$scope.projectId = $routeParams.projectId;
				$scope.showTaskCreationBox = true;
			} else {
				$scope.projectId = 0;
				$scope.showTaskCreationBox = false;
			}
			$scope.clearAllFilters = function() {
				$scope.late = "";
				$scope.commingDeadline = "";
				$scope.status = [];
				$scope.priority = [];
				$scope.filterTags = [];
				$scope.getTasks();
			}
			$scope.getTasks = function() {

				$http
						.post(
								'/all/task/getmytasks/?status=' + $scope.status
										+ '&priority=' + $scope.priority
										+ '&projectId=' + $scope.projectId
										+ '&deadline=' + $scope.late+'&userid='+$routeParams.id,
								$scope.filterTags).success(function(response) {
debugger
							$scope.taskss = response;

						}).error(function(error) {
						})

			}

			$scope.getTasks();

			$rootScope.projectId = $routeParams.id;
			$scope.createNewTask = function() {

				$scope.task.project = {
					"projectId" : $routeParams.projectId
				};

				if ($routeParams.projectId) {

					createtask.task($scope, $scope.task).success(function() {
						toaster.method("success", "Success!!", ngToast);
						$scope.getTasks();
						$scope.task.title = null;

					}).error(function(err) {

					})

				} else {
					toaster.method("danger", "Please select a project!!",
							ngToast);
				}

			}

			tmsutility.method($scope);
			$scope.searchTask = "";
			$scope.loadTasks = function() {
				$http({
					method : 'GET',
					url : '/all/task/searchtasks?value=' + $scope.searchTask
				}).success(function(response) {
					$scope.tasks = response.data;
				}).error(function(err) {
					$log.info(err);
				});
			}

			if ($routeParams.id) {
				$rootScope.projectfield = false;
			} else {
				$rootScope.projectfield = true;
			}
			// $scope.getTaskByProject();
			$scope.edit = function(index) {
				$rootScope.editedTask = angular.copy($scope.tasks[index]);
				var date = new Date($scope.editedTask.createdDate);
				$scope.editedTask.createdDate = date;
			}

			$scope.projectid = angular.copy($location.search().projectId);

			// Adding tags via JSON

			$scope.deleteTask = function(index) {

				// $scope.deletetask = angular.copy($scope.tasks[index])

				$http.post("/all/task/deletetask?taskid=" + index).success(
						function() {
							$scope.getTasks();
							toaster.method("success", "Success!!", ngToast);

						}).error(function(err) {
					$log.info(err);
					ngToast.create({
						className : 'danger',
						content : 'Error!!'
					});
				})
			}

			$scope.getTags = function() {
				$http.get(
						'/all/task/gettags?searchletter='
								+ $scope.customSelected + '&projectId='
								+ $routeParams.projectId).success(
						function(res) {
							$scope.tags = res;

						}).error(function(err) {
					$log.info(err)
				})
			}
		})
