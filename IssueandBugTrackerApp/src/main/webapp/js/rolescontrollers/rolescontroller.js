/**
 * @author deepanjan
 */
// List of authorities
tmsApp.factory('authorities', function($http, $log) {
	return {
		getAuthorities : function($scope) {

			$http.get('/admin/projectauthorities/getauthorities').success(
					function(response) {

						$scope.projectauthorities = [];
						angular.forEach(response, function(key, value) {

							$scope.projectauthorities.push({
								"name" : key,
								"authority" : value
							})
						})
					}).error(function(err) {
				$log.info(err);
			})
		}
	}

})

tmsApp
		.controller('rolescontroller', function($scope, $http, tmsutility,
				authorities, toaster, ngToast, $log) {

			tmsutility.method($scope);
			authorities.getAuthorities($scope);
			$scope.authorities = []

			$scope.toggleAllCheckBoxes = function(event) {
				if (event.currentTarget.value == "addroles") {
					var toggleStatus = $scope.isAllSelected;
				} else {
					var toggleStatus = $scope.allSelected;
				}
				angular.forEach($scope.projectauthorities, function(itm) {
					if (event.currentTarget.value == "addroles") {
						itm.selected = toggleStatus;
					} else {
						itm.select = toggleStatus
					}

					if (event.currentTarget.checked) {

						$scope.authorities.push(itm

						)
					} else {
						$scope.authorities = []
					}
				});

			}

			$scope.isInvalid = function(input) {
				if ($scope.roleform.$submitted) {
					var ele = $scope.roleform[input].$invalid;
					return ele ? 'has-error' : '';
				}
			}

			$scope.isFormInvalid = function() {
				return $scope.roleform.$invalid;
			}

			$scope.passRoleId = function(rol) {

				$scope.role = rol;
				$scope.editedroleName = angular.copy(rol.role)
			}

			$scope.toggleAuthority = function(event, auth) {

				if (event.currentTarget.checked) {
					$scope.authorities.push(auth

					);
				}

				if (!event.currentTarget.checked) {

					var index = $scope.authorities.indexOf(auth)
					$scope.authorities.splice(index, 1);
					$scope.allSelected = false
					$scope.isAllSelected = false
				}
			}

			$scope.addRoles = function() {
				$scope.roleAuthorities = []
				angular.forEach($scope.authorities, function(obj) {
					$scope.roleAuthorities.push({
						"authority" : obj.authority
					})
				})
				$scope.projectRoles = {
					role : $scope.roleName,
					authorities : $scope.roleAuthorities,
				}

				if (!$scope.isFormInvalid()&& $scope.roleAuthorities.length>0) {
					$http.post('/admin/projectauthroles/save',
							$scope.projectRoles)
							.success(
									function() {
										$scope.roleform.$submitted = false;
										$scope.roleName = null;
										$scope.isAllSelected = false;

										angular.forEach(
												$scope.projectauthorities,
												function(itm) {
													itm.selected = false;
												});
										$scope.getrolesandauthorities();
										$scope.projectRoles = {
											role : " ",
											authorities : [],
										};
										toaster.method("success", "Success!!",
												ngToast);
									}).error(function(err) {
								toaster.method("danger", "Error!!", ngToast);
								$log.info(err);
							})
				}else{
					toaster.method("danger", "select the authorities", ngToast);
				}
			}

			$scope.getrolesandauthorities = function() {
				$http.get('/admin/projectauthroles/getr&a').success(
						function(response) {

							$scope.projectrolesandauthorities = response;
						}).error(function(err) {
					$log.info(err);
				})
			}

			$scope.updateProjectRole = function() {
				$scope.editedRoleAuthorities = []
				angular.forEach($scope.authorities, function(obj) {
					$scope.editedRoleAuthorities.push({
						"authority" : obj.authority
					})
				})
				$scope.projectRoles = {
					id : $scope.role.id,
					role : $scope.editedroleName,
					authorities : $scope.editedRoleAuthorities,
				}

				$http.post('/admin/projectauthorities/updater&a',
						$scope.projectRoles).success(function() {
					$scope.authorities = []
					$scope.allSelected = false;
					$scope.getrolesandauthorities();
					angular.forEach($scope.projectauthorities, function(obj) {
						obj.select = false
						obj
					})

				}).error(function(err) {
					$log.info(err);
				})

			}

			$scope.getrolesandauthorities();

		})