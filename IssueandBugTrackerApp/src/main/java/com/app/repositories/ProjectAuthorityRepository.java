package com.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.ProjectAuthority;
import com.app.entities.ProjectRole;

public interface ProjectAuthorityRepository extends
		JpaRepository<ProjectAuthority, String> {

	List<ProjectAuthority> findByRole(ProjectRole role);

	List<ProjectAuthority> findByRoleId(String id);

	ProjectRole findById(String id);

}
