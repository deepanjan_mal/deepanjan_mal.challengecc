package com.app.auth.controller;
/**
 * @author Deepanjan
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.auth.services.AuthService;
import com.app.entities.Project;
import com.app.entities.Tenant;
import com.app.entities.User;
import com.app.entities.UserProjectRole;
import com.app.entities.UserRole;
import com.app.repositories.ProjectRepository;
import com.app.repositories.TenantRepository;
import com.app.repositories.UserProjectRolesRepository;
import com.app.repositories.UserRepository;
import com.app.repositories.UserRoleRepository;
import com.app.util.AuthUtilService;
import com.app.util.DefaultRestException;
import com.app.util.views.LoadUserJsonVew;
import com.app.util.views.UserJsonView;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class AuthController {

	@Autowired
	AuthService authService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	TenantRepository tenantRepository;

	@Autowired
	AuthUtilService authUtilService;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	UserProjectRolesRepository userProjectRolesRepository;

	@RequestMapping("/public/login")
	@JsonView(UserJsonView.class)
	public User getLogin() {
		return authService.getLogin();
	}

	@RequestMapping("/admin/adduser")
	public Map<String, Object> addUser(@RequestBody User user) throws Exception {
		return authService.addUser(user);
	}

	@RequestMapping("/admin/updateUser")
	@JsonView(UserJsonView.class)
	public void updateUser(@RequestBody User user) {
		authService.updateUser(user);
	}

	@RequestMapping("/admin/getuseredit")
	@JsonView(UserJsonView.class)
	public User getUser(@RequestParam String id) {
		return authService.getEditUser(id);
	}

	@RequestMapping("/admin/getRoles")
	public List<UserRole> getRoles() {
		return authService.getRoles();
	}

	@RequestMapping("/all/getUsers")
	@JsonView(UserJsonView.class)
	public List<User> getUsers(@RequestParam("value") String name) {
		return authService.getUsers(name);
	}

	@RequestMapping("/all/loadusers")
	@JsonView(LoadUserJsonVew.class)
	public List<User> loadUsers(@RequestParam("query") String name,
			@RequestParam long projectId) {
		Project project = projectRepository.findByProjectIdAndTenant(projectId,
				authUtilService.getLoggedInUsersTenant());
		List<UserProjectRole> usersList = userProjectRolesRepository
				.findByProject(project);
		List<User> userList = new ArrayList<User>();
		for (UserProjectRole user : usersList) {

			userList.add(user.getUser());

		}

		return userList;
	}

	@RequestMapping("/public/request/forgetpassword")
	public void requestForgetPassword(@RequestBody HashMap<Object, Object> email) {
		authService.requestForgetPassword(email);
	}

	@RequestMapping("/public/tenantregistration")
	public Map<String, Object> register(@RequestBody HashMap<Object, Object> map)
			throws Exception {
		ObjectMapper objMap = new ObjectMapper();
		Tenant tenant = objMap.convertValue(map.get("tenant"), Tenant.class);
		User user = objMap.convertValue(map.get("user"), User.class);
		return authService.registerTenant(user, tenant);
	}

	@RequestMapping("/public/tenant/url")
	public Tenant getLink(@RequestParam String tenantDomain) {
		return authService.getLink(tenantDomain);
	}

	@RequestMapping("/public/tenant/domain")
	public Tenant getDomain(@RequestParam String tenantName) {
		return authService.getDomain(tenantName);
	}

	@RequestMapping("/public/userregistration")
	public Map<String, Object> registerUser(
			@RequestBody HashMap<Object, Object> user,
			@RequestParam String domain) throws DefaultRestException {
		ObjectMapper objMap = new ObjectMapper();
		User userObj = objMap.convertValue(user, User.class);
		return authService.registerUser(userObj, domain);
	}

	@RequestMapping("/admin/deactivateuser")
	@JsonView(UserJsonView.class)
	public void deactivateUser(@RequestBody String userid) {
		authService.deactivateUser(userid);
	}

	@RequestMapping("/admin/activateuser")
	@JsonView(UserJsonView.class)
	public void activateUser(@RequestBody String userid) {
		authService.activateUser(userid);
	}

	@RequestMapping("/public/checkforemail")
	@JsonView(UserJsonView.class)
	public Map<String, Object> checkForEmail(@RequestBody User usermail)
			throws Exception {
		return authService.checkForEmail(usermail);
	}

	@RequestMapping("/admin/reset/pass")
	@JsonView(UserJsonView.class)
	public Map<String, Object> resetPassword(@RequestParam String user)
			throws Exception {
		return authService.resetPassword(user);
	}

	@RequestMapping("/admin/resend/act")
	@JsonView(UserJsonView.class)
	public Map<String, Object> resendActivation(@RequestParam String user)
			throws Exception {
		return authService.resendActivation(user);
	}

	@RequestMapping("/all/getUserDetail")
	@JsonView(UserJsonView.class)
	public User getUserDetail(@RequestParam String username) throws Exception {
		return authService.getUserDetail(username);
	}
}