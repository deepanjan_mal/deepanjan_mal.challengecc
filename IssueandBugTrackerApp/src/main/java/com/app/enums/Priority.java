
package com.app.enums;

/**
 * @author Deepanjan
 */
public enum Priority {

	HIGH("High"), MEDIUM("Medium"), LOW("Low"), CRITICAL("Critical");

	String value;

	/**
	 * @param value
	 */
	private Priority(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public String toString() {
		return this.value;
	}
}
