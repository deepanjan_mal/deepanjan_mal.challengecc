tmsApp
		.controller(
				'projectcontroller',
				[
						'$scope',
						'tmsutility',
						'$rootScope',
						'$http',
						'getusersservice','$log',
						function($scope, tmsutility, $rootScope, $http,
								getusersservice, $log) {
							tmsutility.method($scope);
							$scope.getProjects = function() {

								$http(
										{
											method : 'GET',
											url : '/all/project/getprojects?value=null'
										}).success(function(response) {
									$scope.projects = response;
									
								}).error(function(err){
									$log.info(err);
								});
							}

							$scope.getProjects();

						
							$scope.edit = function(index) {
								$rootScope.editedProject = angular
										.copy($scope.projects[index]);
								var date = new Date(
										$scope.editedProject.startDate);
 								$scope.editedProject.startDate = date;
							}
							
							$scope.loadProjects = function() {
								$http(
										{
											method : 'GET',
											url : '/all/project/getprojects?value='
													+ $scope.searchText,
										}).success(function(response) {
									$scope.projects = response.data;
								}).error(function(err){
									$scope.getProjects();
									$log.info(err);
								})
							}

				} ])
