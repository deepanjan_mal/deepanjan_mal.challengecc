package com.app.project.controller;
/**
 * @author deepanjan
 */
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.ProjectRole;
import com.app.entities.User;
import com.app.entities.UserProjectRole;
import com.app.enums.Authority;
import com.app.project.services.ProjectRoleService;
import com.app.util.AuthUtilService;
import com.app.util.ProjectAuthUtilities;
import com.app.util.views.ProjectJsonView;
import com.app.util.views.ProjectRoleJsonView;
import com.app.util.views.UserProjectRoleJsonView;
import com.app.util.views.UserTenantJsonView;
import com.app.util.views.UserProjectRoleJsonView.UserprojectrolesJsonview;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class ProjectRoleController {

	@Autowired
	ProjectRoleService projectRoleService;

	@Autowired
	ProjectAuthUtilities projectAuthUtilities;

	@Autowired
	AuthUtilService authUtilService;

	@RequestMapping("/admin/projectauthroles/getr&a")
	@JsonView({ ProjectRoleJsonView.class })
	public List<ProjectRole> getProjectRolesAndAuthorities() {
		return projectRoleService.getProjectRolesAndAuthorities();
	}

	@RequestMapping("/admin/projectauthroles/save")
	public void addRoleAndAuthorities(@RequestBody ProjectRole projectRole)
			throws ParseException {
		projectRoleService.addRoleAndAuthorities(projectRole);
	}

	@RequestMapping("/all/project/userprojectroles")
	@JsonView({ ProjectJsonView.class })
	public void addUserProjectRoles(@RequestBody UserProjectRole userProjectRole)
			throws Exception {
		User user = authUtilService.getLoggedInUser();
		if (!user.getRoles().get(0).getRoleType().equals("admin")) {
			projectAuthUtilities.checkForAuthority(Authority.ADD_MEMBER,
					userProjectRole.getProject());
			projectRoleService.addUserProjectRoles(userProjectRole);

		} else
			projectRoleService.addUserProjectRoles(userProjectRole);
	}

	@RequestMapping("/all/project/updateuserprojectroles")
	@JsonView({ ProjectJsonView.class })
	public void updateUserProjectRoles(
			@RequestBody UserProjectRole userProjectRole) throws Exception {
		projectRoleService.updateUserProjectRoles(userProjectRole);
	}

	@RequestMapping("/all/project/deleteuserprojectroles")
	@JsonView({ ProjectJsonView.class })
	public void deleteUserProjectRoles(
			@RequestBody UserProjectRole userProjectRole) throws Exception {
		projectRoleService.deleteUserProjectRoles(userProjectRole);
	}

	@RequestMapping("/all/project/getRoles")
	@JsonView({ ProjectJsonView.class })
	public List<ProjectRole> getProjectRoles() {
		return projectRoleService.getProjectRoles();
	}

	@RequestMapping("/all/project/getuserprojectroles")
	@JsonView({ UserProjectRoleJsonView.class })
	public List<UserProjectRole> getUserProjectRoles(
			@RequestParam("value") long id) {
		return projectRoleService.getUserProjectRoles(id);
	}

	@RequestMapping("/admin/projectauthorities/getauthorities")
	public HashMap<Authority, String> getAuthorities() {
		return projectRoleService.getAuthorities();
	}

	@RequestMapping("/admin/projectauthorities/updater&a")
	public void updateRolesAndAuthorities(@RequestBody ProjectRole role) {
		projectRoleService.updateRolesAndAthorities(role);
	}

	@RequestMapping("/all/getprojectroleauthorities")
	@JsonView(UserprojectrolesJsonview.class)
	public HashMap<Authority, Boolean> getProjectRoleAthorities(
			@RequestParam long projectId) {
		return projectRoleService.getProjectRoleAuthorities(projectId);
	}

	@RequestMapping("/all/project/getUserForProject")
	@JsonView(UserTenantJsonView.class)
	public List<User> getUserForProject(@RequestParam long value) {
		return projectRoleService.getUserForProject(value);
	}

}
