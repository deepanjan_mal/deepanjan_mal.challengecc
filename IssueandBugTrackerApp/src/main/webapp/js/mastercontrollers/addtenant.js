tmsApp.controller('addtenantcontroller', function($scope, $http, $log) {

	$scope.addTenant = function() {
		$scope.addtenant = {
			tenant : $scope.tenant,
			user : $scope.user
		}
		$http.post('/sa/add/tenant/user', $scope.addtenant).success(
				function(response) {
					location.href = "#/viewtenant";
					if (response.success != null) {
						toaster.method("success", response.success, ngToast);
					} else {
						toaster.method("danger", "Tenant addition failed",
								ngToast);
					}
				})
	}
})