/**
 * 
 */
tmsApp.controller('adduserstotenantcontroller', function($routeParams, $scope,
		$rootScope, $http, $log) {
	$rootScope.isAuthenticated = false;
	$rootScope.isActivatedUser = true;

	$http.get('/public/tenant/url?tenantDomain=' + $routeParams.domain)
			.success(function(response) {
				
				$scope.domainLink = response.tenantUrl;
			}).error(function(err) {
				$log.info(err);
			})
})