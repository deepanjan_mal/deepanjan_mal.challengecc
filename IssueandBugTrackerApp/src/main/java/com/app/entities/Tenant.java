package com.app.entities;
/**
 * @author Deepanjan
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.app.util.views.TenantConfigurationView;
import com.app.util.views.UserJsonView;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "tenants")
public class Tenant implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "tenant_id")
	@JsonView({ UserJsonView.class, TenantConfigurationView.class})
	long tenantId;

	@Column(name = "tenant_name",  unique=true)
	@JsonView({ UserJsonView.class, TenantConfigurationView.class})
	String tenantName;

	@Column(name = "domain", unique=true)
	@JsonView({ UserJsonView.class, TenantConfigurationView.class})
	String domain;
	
	@Column(name = "tenant_url", unique=true)
	@JsonView(TenantConfigurationView.class)
	String tenantUrl;
	

	public String getTenantUrl() {
		return tenantUrl;
	}


	public void setTenantUrl(String tenantUrl) {
		this.tenantUrl = tenantUrl;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public long getTenantId() {
		return tenantId;
	}


	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	@Override
	public String toString() {
		return "Tenant [tenantName=" + tenantName + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result
				+ ((tenantName == null) ? 0 : tenantName.hashCode());
		result = prime * result + (int) (tenantId ^ (tenantId >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tenant other = (Tenant) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (tenantName == null) {
			if (other.tenantName != null)
				return false;
		} else if (!tenantName.equals(other.tenantName))
			return false;
		if (tenantId != other.tenantId)
			return false;
		return true;
	}

}
